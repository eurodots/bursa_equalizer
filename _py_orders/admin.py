# CRYPTOPIA: 1
from cryptopia.pairs import getPairs as pairsCryptopia
from cryptopia.orders import getOrders as getOrdersCryptopia

# BINANCE: 2
from binance.pairs import getPairs as pairsBinance
from binance.orders import getOrders as getOrdersBinance

# LIQUI: 3
from liqui.pairs import getPairs as pairsLiqui
from liqui.orders import getOrders as getOrdersLiqui

# MERCATOX: 22
from metcatox.pairs import getPairs as pairsMercatox
from metcatox.orders import getOrders as getOrdersMercatox

# ———————————————————————————————————————————————————
from _utils.pairsToDB import *
from _utils.ordersToDB import saveOrdersToDB
# ———————————————————————————————————————————————————




def stock(name):
    stocks_arr = {
        'cryptopia': 1,
        'binance': 2,
        'liqui': 3,
        'mercatox': 20,
    }

    return {'stock_id': stocks_arr[name], 'stock_name': name}


def updateAllPairs():
    pairsUpdate(stock('binance'), pairsBinance())
    pairsUpdate(stock('mercatox'), pairsMercatox())
    pairsUpdate(stock('cryptopia'), pairsCryptopia())
    pairsUpdate(stock('liqui'), pairsLiqui())
    pass


def downloadAllOrders():
    # getOrdersBinance(loadPairs(stock('binance')))
    getOrdersMercatox(loadPairs(stock('mercatox')))
    getOrdersCryptopia(loadPairs(stock('cryptopia')))
    getOrdersLiqui(loadPairs(stock('liqui')))
    pass

def saveAllOrdersToDB():
    saveOrdersToDB()


def main():

    commands = ({
        'Update all pairs': 1,
        'Download all orders': 2,
        'Save all orders to DB': 3,
        # 'Get orders from Binance': 3,
    })

    getCommand = False
    commands_arr = 'Input command\n'
    for key in commands.keys():
        commands_arr += key + ': [' + str(commands[key]) + ']\n'


    getCommand = int(input(commands_arr))
    if getCommand == 1:
        updateAllPairs()
    elif getCommand == 2:
        downloadAllOrders()
    elif getCommand == 3:
        saveAllOrdersToDB()
    # elif getCommand == 3:
    #     getOrdersAll()
    else:
        print('Command ['+str(getCommand)+'] not found')



if __name__ == '__main__':
    # updatePairs()
    main()
    # getPairsAll()
    # getOrdersAll()
    # getPairsAll()