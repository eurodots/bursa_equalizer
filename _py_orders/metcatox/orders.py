import threading
import cfscrape
from bs4 import BeautifulSoup

# ————————————————————————————————————————————
from _utils._functions import *
from _utils.ordersToDB import saveOrdersToJson
# ————————————————————————————————————————————


def getOrders(data):

    data_split = split(data['pairs_arr'], 20)
    for d in data_split:
        arr = data
        arr['pairs_arr'] = d
        splitter(arr)


def splitter(data):

    ev = {}
    th = {}
    for index, pair in enumerate(data['pairs_arr']):

        # if index < 3:

        args = (data['stock_id'], data['stock_name'], pair)

        ev["x" + str(index)] = threading.Event()
        th["x" + str(index)] = threading.Thread(target=parser, args=args)
        th["x" + str(index)].start()

        # exit()

    ev["x1"].set()
    for i, t in enumerate(th):
        th["x" + str(i)].join()




def parser(stock_id, stock_name, pair):

    sufix = pair['coin_from']+'/'+pair['coin_to']
    url = 'https://mercatox.com/exchange/'+sufix
    print(url)


    scraper = cfscrape.create_scraper()
    site = scraper.get(url, headers=sysConf['header']).content
    soup = BeautifulSoup(site, "lxml")

    orders_arr = {
        'sell': [],
        'buy': []
    }

    orders_sell = soup.find('div', {'class': 'left_order_book_page'}).find_all('div', {'class': 'row_fix'})
    for item in orders_sell:
        orders_arr['sell'].append([item.get('price'), item.get('amount')])

    orders_buy = soup.find('div', {'class': 'right_order_book_page'}).find_all('div', {'class': 'row_fix'})
    for item in orders_buy:
        orders_arr['buy'].append([item.get('price'), item.get('amount')])

    response_arr = {
        'stock_name': stock_name,
        'stock_id': stock_id,
        'pair_id': pair['pair_id'],
        'pair_label': pair['pair_label'],
        'updated_at': timestamp(),
        'orders': orders_arr,
    }

    saveOrdersToJson(response_arr)