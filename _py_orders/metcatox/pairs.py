import json
import requests

# ————————————————————————————————————————————
from _utils.config import *
# ————————————————————————————————————————————

def getPairs():

    url = 'https://mercatox.com/public/json24'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    allPairs = []
    for item in data['pairs']:
        coin = []
        coin = item.split('_')

        allPairs.append([coin[0], coin[1]])

    return allPairs


