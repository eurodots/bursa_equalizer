import threading
import requests
import json
from operator import itemgetter

# ————————————————————————————————————————————
from _utils._functions import *
from _utils.ordersToDB import saveOrdersToJson
# ————————————————————————————————————————————


def getOrders(data):

    data_split = split(data['pairs_arr'], 20)
    for d in data_split:
        arr = data
        arr['pairs_arr'] = d
        splitter(arr)


def splitter(data):

    ev = {}
    th = {}
    for index, pair in enumerate(data['pairs_arr']):

        # if index < 3:

        args = (data['stock_id'], data['stock_name'], pair)

        ev["x" + str(index)] = threading.Event()
        th["x" + str(index)] = threading.Thread(target=parser, args=args)
        th["x" + str(index)].start()

            # exit()

    ev["x1"].set()
    for i, t in enumerate(th):
        th["x" + str(i)].join()




def parser(stock_id, stock_name, pair):


    url = 'https://www.cryptopia.co.nz/api/GetMarketOrderGroups/' + (pair['coin_from']+'_'+pair['coin_to'])
    print(url)

    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))
    data = data['Data'][0]


    orders_arr = {
        'sell': [],
        'buy': []
    }

    for order in data['Sell']:
        orders_arr['sell'].append([order['Price'], order['Volume']])

    for order in data['Buy']:
        orders_arr['buy'].append([order['Price'], order['Volume']])

    response_arr = {
        'stock_name': stock_name,
        'stock_id': stock_id,
        'pair_id': pair['pair_id'],
        'pair_label': pair['pair_label'],
        'updated_at': timestamp(),
        'orders': orders_arr,
    }

    saveOrdersToJson(response_arr)
    # print(response_arr)
