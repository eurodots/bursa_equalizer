import json
import requests

# ————————————————————————————————————————————
from _utils.config import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://www.cryptopia.co.nz/api/GetTradePairs'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['Data']:

        label = pair['Label']
        label = label.split('/')

        allPairs.append([label[0], label[1]])

    return allPairs