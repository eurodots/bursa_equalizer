import threading
import websocket
# import asyncio
import ssl
import json

from _utils._functions import *
from _utils.ordersToDB import saveOrdersToJson


def getOrders(data):

    # for pair in data['pairs_arr']:
    #     print( connectSocket(data['stock_id'], data['stock_name'], pair) )
        # print(data['stock_id'], data['stock_name'], pair)
        # exit()

    # asyncio.ensure_future(connectSocket())

    ev = {}
    th = {}
    for index, pair in enumerate(data['pairs_arr']):

        if index < 3:

            args = (data['stock_id'], data['stock_name'], pair)

            ev["x" + str(index)] = threading.Event()
            th["x" + str(index)] = threading.Thread(target=connectSocket, args=args)
            th["x" + str(index)].start()

            # exit()

    ev["x1"].set()
    for i, t in enumerate(th):
        th["x" + str(i)].join()



def connectSocket(stock_id, stock_name, pair):

    ws_pair = pair['coin_from'].lower()+pair['coin_to'].lower()
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws.connect('wss://stream.binance.com:9443/ws/'+ws_pair+'@depth20')

    response_arr = []

    while True:
        time.sleep(1)

        response = json.loads(ws.recv())
        orders_arr = {
            'sell': [],
            'buy': []
        }
        for resp in response:

            type = ''
            if resp == 'asks':
                type = 'sell'
            elif resp == 'bids':
                type = 'buy'

            if type in ['sell','buy']:
                orders = response[resp]

                for order in orders:
                    order_tmp = [order[0], order[1]]
                    # order_tmp['hash_id'] = hashGenerator(order_tmp)
                    orders_arr[type].append(order_tmp)

        response_arr = {
            'stock_name': stock_name,
            'stock_id': stock_id,
            'pair_id': pair['pair_id'],
            'pair_label': pair['pair_label'],
            'updated_at': timestamp(),
            'orders': orders_arr,
        }

        saveOrdersToJson(response_arr)



def d():
    print('--------')

