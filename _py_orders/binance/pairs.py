import websocket
import ssl
import json


def getPairs():
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws.connect('wss://stream.binance.com:9443/ws/!ticker@arr')

    msg = json.loads(ws.recv())

    tickers = []
    for pair in msg:
        coin = pair['s']
        if coin[-4:] == 'USDT':
            tickers.append([coin[0:-4], coin[-4:]])
        else:
            tickers.append([coin[0:-3], coin[-3:]])


    return tickers


