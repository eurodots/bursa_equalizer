dbConf = {
    'host': '127.0.0.1',
    'port': 8889,
    'user': 'root',
    'passwd': 'root',
    'name': 'bittrade',
    'charset': 'utf8'
}

# dbConf = {
#     'host': '127.0.0.1',
#     'port': 3306,
#     'user': 'admin_kupinet',
#     'passwd': '353535',
#     'name': 'admin_kupinet',
#     'charset': 'utf8'
# }


sysConf = {
    'timeZone': 'GMT0',
    "header": {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
        'content-type': 'text',
    },
}
