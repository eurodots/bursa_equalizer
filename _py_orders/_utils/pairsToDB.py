import pymysql
import os
import json


from _utils.config import dbConf




def pairsUpdate(data, arr):


    pairs_arr = []


    # GET PAIR'S ID FROM DB
    conn = pymysql.connect(host=dbConf['host'], port=dbConf['port'], user=dbConf['user'], passwd=dbConf['passwd'], db=dbConf['name'], charset=dbConf['charset'])
    cur = conn.cursor()

    for index, coin in enumerate(arr):
        try:
            query = ([
                data['stock_id'],
                coin[0],
                coin[1]
            ])
            cur.execute("SELECT pair.id FROM stock_pair JOIN pair ON stock_pair.pair_id = pair.id WHERE stock_pair.stock_id = %s AND pair.coin_from = %s AND pair.coin_to = %s", query)
            pair_id = cur.fetchone()
            pair_id = pair_id[0] if pair_id else False

            if not pair_id:
                cur.execute("INSERT INTO `pair` (`coin_from`,`coin_to`) VALUES (%s, %s)", coin)
                pair_id = cur.lastrowid

                query = ([
                    pair_id,
                    '_'.join(coin),
                    data['stock_id'],
                    data['stock_name'],
                    1 # Whitelist True
                ])
                cur.execute("INSERT INTO `stock_pair` (`pair_id`,`pair_label`,`stock_id`,`stock_name`,`whitelist`) VALUES (%s, %s, %s, %s, %s)", query)
                conn.commit()


            pair_label = [coin[0].upper(), coin[1].upper()]
            pairs_arr.append({
                'pair_label': '_'.join(pair_label),
                'coin_from': pair_label[0],
                'coin_to': pair_label[1],
                'pair_id': pair_id
            })


        except Exception as e:
            print('>>>ERROR:', e)

    conn.close()

    merge_arr = {
        'stock_id': data['stock_id'],
        'stock_name': data['stock_name'],
        'pairs_arr': pairs_arr
    }
    savePairsToFile(data, merge_arr)


def savePairsToFile(data, pairs_arr):
    filename = str(data['stock_id'])+'_'+data['stock_name']
    path = '_data/pairs/'+filename+'.json'

    with open(path, 'w') as outfile:
        json.dump(pairs_arr, outfile)



def loadPairs(data):
    folder = "_data/pairs/"+str(data['stock_id'])+'_'+data['stock_name']+'.json'

    checkFile = os.path.isfile(folder)
    if checkFile:
        return json.load(open(folder))
    else:
        print('PAIRS NOT EXIST!', data)
        exit()
        return False




def main():
    pass


if __name__ == "__main__":
    main()