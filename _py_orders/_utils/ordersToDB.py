import datetime
import time
import pymysql
import os
import json

# from _utils._functions import *
from _utils.config import dbConf


def saveOrdersToJson(data_arr):


    folder_stock = '_data/orders/'+data_arr['stock_name']
    file_pair = data_arr['pair_label']+'.json'

    if not os.path.exists(folder_stock):
        os.makedirs(folder_stock)

    path = os.path.join(folder_stock, file_pair)
    with open(path, 'w') as outfile:
        json.dump(data_arr, outfile)

    print(path)



def saveOrdersToDB():

    folder = '_data/orders/'
    files_arr = []

    for root, directories, filenames in os.walk(folder):
        for filename in filenames:
            if not filename.startswith('.'):
                files_arr.append( os.path.join(root, filename) )



    conn = pymysql.connect(host=dbConf['host'], port=dbConf['port'], user=dbConf['user'], passwd=dbConf['passwd'], db=dbConf['name'], charset=dbConf['charset'])
    cur = conn.cursor()


    for f in files_arr:
        orders_arr = json.load(open(f))

        print(f)
        os.unlink(f)

        orders_sell = []
        for order in orders_arr['orders']['sell']:
            orders_sell.append([
                format(float(order[0]), '.8f'),
                format(float(order[1]), '.8f'),
            ])
        orders_sell = json.dumps(orders_sell)

        orders_buy = []
        for order in orders_arr['orders']['buy']:
            orders_buy.append([
                format(float(order[0]), '.8f'),
                format(float(order[1]), '.8f'),
            ])
        orders_buy = json.dumps(orders_buy)

        query = ([
            orders_arr['stock_id'],
            orders_arr['pair_id']
        ])
        cur.execute("SELECT id FROM stock_order WHERE stock_id = %s AND pair_id = %s", query)
        order_id = cur.fetchone()
        order_id = order_id[0] if order_id else False


        try:

            if order_id:
                query = ([
                    orders_sell,
                    orders_buy,
                    orders_arr['updated_at'],
                    order_id
                ])
                cur.execute("UPDATE `stock_order` SET `arr_sell` = %s, `arr_buy` = %s, `updated_at` = %s WHERE `id` = %s", query)
                conn.commit()

            else:
                query = ([
                    orders_arr['stock_id'],
                    orders_arr['stock_name'],
                    orders_arr['pair_id'],
                    orders_arr['pair_label'],
                    orders_sell,
                    orders_buy,
                    orders_arr['updated_at'],
                ])
                cur.execute("INSERT INTO `stock_order` (`id`,`stock_id`,`stock_name`,`pair_id`,`pair_label`,`arr_sell`,`arr_buy`,`updated_at`) VALUES (Null,%s,%s,%s,%s,%s,%s,%s)", query)
                conn.commit()



        except Exception as e:
            print('>>>ERROR:', e)

    conn.close()











def main():
    # getJsonOrders()
    # saveOrders()
    pass

if __name__ == "__main__":
    main()