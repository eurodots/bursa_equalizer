import json
import requests

# ————————————————————————————————————————————
from _utils.config import *
# ————————————————————————————————————————————


def getPairs():
    url = 'https://api.liqui.io/api/3/info'
    r = requests.get(url, headers=sysConf['header'])
    data = json.loads(r.content.decode('utf8'))

    allPairs = []

    for pair in data['pairs']:

        label = pair.upper()
        label = label.split('_')

        allPairs.append([label[0], label[1]])

    return allPairs