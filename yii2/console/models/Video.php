<?php
namespace console\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;
use zhelyabuzhsky\sitemap\models\SitemapEntityInterface;


class Video extends ActiveRecord implements SitemapEntityInterface
{
    public static function tableName()
    {
        return "video";
    }

    const WEEKLY = 'weekly';
    const DAILY = 'daily';
    const ALWAYS = 'always';
    const HOURLY = 'hourly';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
    const NEVER = 'never';

    /**
     * @inheritdoc
     */
    public function getSitemapLastmod()
    {
        return date('Y-m-d', strtotime($this->updated_at));
    }

    /**
     * @inheritdoc
     */
    public function getSitemapChangefreq()
    {
        return self::WEEKLY;
    }

    /**
     * @inheritdoc
     */
    public function getSitemapPriority()
    {
        return '0.8';
    }

    /**
     * @inheritdoc
     */
    public static function getSitemapDataSource()
    {
        $array = Video::find();
        return $array;
    }

    /**
     * @inheritdoc
     */
    public function getSitemapLoc()
    {
        $url = "/watch?v=".$this->identity;
        return Url::to($url, true);
    }

}
