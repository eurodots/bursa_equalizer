<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

use console\models\Video;

class SitemapController extends Controller
{
    public function actionInit()
    {

    }

    public function actionIndex()
    {

      \Yii::$app->sitemap
        ->addModel(Video::className())
        // ->addModel(Video::className(), \Yii::$app->db) // Also you can pass \yii\db\Connection to the database connection that you need to use
        // ->setDisallowUrls([
        //   '#url1#',
        //   '#url2$#',
        // ])
        ->create();
    }
}
