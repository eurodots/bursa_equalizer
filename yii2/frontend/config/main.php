<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'KUPI.NET',
    'homeUrl'=>'/',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'view' => [
            'class' => '\ogheo\htmlcompress\View',
            'compress' => YII_ENV_DEV ? false : true,
            // ...
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js'=>[]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => []
                ]
            ]
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'hostInfo' => 'http://bursadex.com:8888',
            'baseUrl' => '/',
            'scriptUrl' => '',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            // 'scriptUrl'=>'/frontend/web/index.php',

            'rules' => [
                '/' => 'site/index',

                // page routes
                // 'watch' => 'site/watch',
                // '<controller:(watch)>' => 'site/watch',
                // 'watch'=>'site/watch',
                // 'download' => 'site/download',

                // '<controller:(watch)>' => 'site/watch',
                // '<controller:\w+>/<id:\d+>' => '<controller>/view',
                // '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                // '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                //
                // // Remove 'site' parameter from URL
                // '<action:(.*)>' => 'site/<action>',

                // 'search' => 'site/index',
                // 'login' => 'site/index',


                // '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
                // '<controller>' => '<controller>/index',
                // '<controller>/<action>' => '<controller>/<action>',
                // '<modules>/<controller>/<action>' => '<modules>/<controller>/<action>',
                // 'watch' => 'site/watch',

            ],
        ],
        // 'db' => [
        //     'class' => 'yii\db\Connection',
        //     // 'dsn' => 'mysql:host=localhost;dbname=yii2_test',
        //     'dsn' => 'mysql:host=localhost;dbname=youtube',
        //     'username' => 'root',
        //     'password' => 'root',
        //     'charset' => 'utf8',
        //
        //     // Schema cache options (for production environment)
        //     //'enableSchemaCache' => true,
        //     //'schemaCacheDuration' => 60,
        //     //'schemaCache' => 'cache',
        // ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=bursadex',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
        ],
    ],
    'params' => $params,
];
