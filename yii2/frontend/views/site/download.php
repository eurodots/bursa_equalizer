<?php

/* @var $this yii\web\View */
$title = "Download video and audio from Youtube | ForYoutube";
$description = "Online download videos from YouTube for FREE to PC, mobile. Download all types Youtube videos including vevo videos or age protected videos.";

$this->title = $title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);

// print_r($model->language->iso);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $title.' | '.Yii::$app->name
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $description
]);


?>

<p><?=$description?></p>
