<?php
namespace api\models;
use yii\db\ActiveRecord;


class Pair extends ActiveRecord
{
    public static function tableName()
    {
        return 'pair';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getCoin_from()
    {
        return $this->hasOne(Coin::className(), ['iso' => 'coin_from']);
    }
    public function getCoin_to()
    {
        return $this->hasOne(Coin::className(), ['iso' => 'coin_to']);
    }

}
