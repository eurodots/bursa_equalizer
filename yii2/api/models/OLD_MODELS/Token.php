<?php

// namespace api\models;
//
// use common\models\query\PostQuery;
// use yii\behaviors\TimestampBehavior;
// use yii\db\ActiveRecord;

namespace api\models;
use yii\db\ActiveRecord;
// use yii\db\Expression;

class Token extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_token';
    }

    public function generateToken($expire)
    {
        $this->expired_at = $expire;
        $this->token = \Yii::$app->security->generateRandomString();
    }

    public function fields()
    {
        return [
            'token' => 'token',
            'expired' => function () {
                return date(DATE_RFC3339, $this->expired_at);
            },
        ];
    }

    // public function getUser()
    // {
    //     return $this->hasOne(Users::className(), ['id' => 'user_id']);
    // }

    public static function getUser($token=false) {
        if($token) {
            $query = Token::find()->where(['token' => $token])->one();
            if(count($query) > 0) {
                if( $query->expired_at < time()) {
                    $respond = [
                        "status" => 501,
                        "message" => "Token expired!"
                    ];
                    print_r( json_encode($respond) );
                    exit();
                } else {
                    // $newToken = \Yii::$app->security->generateRandomString()
                    // print_r();

                    // exit();
                    $query->expired_at = time() + 3600 * 24;
                    $query->save();
                    // $respond = [
                    //     "status" => 200,
                    //     "token" => $query->user_id
                    // ];
                    return $query->user_id;
                    // print_r( json_encode($respond) );
                    // exit();
                }
            } else {
                $respond = [
                    "status" => 500,
                    "token" => "Token not found"
                ];
                print_r( json_encode($respond) );
                exit();
            }
        }
    }
}
