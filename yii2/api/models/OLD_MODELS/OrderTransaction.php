<?php
namespace api\models;
use yii\db\ActiveRecord;


class OrderTransaction extends ActiveRecord
{
    public static function tableName()
    {
        return 'order_transaction';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getCoin()
    {
        return $this->hasOne(Coin::className(), ['id' => 'coin_id']);
    }
    public function getAccount_from()
    {
        return $this->hasOne(StockAccount::className(), ['id' => 'stock_account_from'])->with(['stock']);
    }
    public function getAccount_to()
    {
        return $this->hasOne(StockAccount::className(), ['id' => 'stock_account_to'])->with(['stock']);
    }

}
