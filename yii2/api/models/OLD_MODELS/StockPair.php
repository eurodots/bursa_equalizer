<?php
namespace api\models;
use yii\db\ActiveRecord;
// use yii\db\Expression;
// use yii\data\ActiveDataProvider;


class StockPair extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock_pair';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getPair()
    {
        return $this->hasOne(Pair::className(), ['id' => 'pair_id']);
    }

    public function getPair_coin()
    {
        return $this->hasOne(Pair::className(), ['id' => 'pair_id'])->with(['coin_from','coin_to']);
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    public function getNew_orders()
    {
        $date = \Yii::$app->params['lastOrdersDate'];
        // $date = \Yii::app()->params['lastOrdersDate'];

        // $date = strtotime('-40 minutes');
        return $this->hasMany(StockOrder::className(), ['stock_id' => 'stock_id', 'pair_id' => 'pair_id'])->where(['>', 'created_at', $date])->with(['stock']);
    }

    public function getEqualizer()
    {
        $date = \Yii::$app->params['lastOrdersDate'];
        return $this->hasMany(StockOrder::className(), ['stock_id' => 'stock_id', 'pair_id' => 'pair_id'])->where(['>', 'created_at', $date]);
    }

    // public function getStock()
    // {
    //     return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    // }

}
