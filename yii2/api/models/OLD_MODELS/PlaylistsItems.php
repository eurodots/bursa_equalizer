<?php
namespace api\models;
use yii\db\ActiveRecord;


class PlaylistsItems extends ActiveRecord
{
    public static function tableName()
    {
        return 'playlists_items';
    }

    // public function attributeLabels() {
    //     return [
    //         'iso' => 'iso',
    //         'name' => 'name',
    //     ];
    // }
    //
    // public function rules() {
    //     return [
    //         [ ['iso', 'name'], 'required' ],
    //     ];
    // }

    // public function getPlaylist()
    // {
    //     return $this->hasOne(Playlists::className(), ['user_id' => 'user_id']);
    // }
    public function getOwner()
    {
        // $playlist_id = $this::findOne($this->item_id)->playlist_id;
        $owner_id = Playlists::findOne($this->playlist_id)->user_id;
        return $owner_id;
    }
}
