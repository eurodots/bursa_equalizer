<?php
namespace api\models;
use yii\db\ActiveRecord;


class StockAccount extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock_account';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    public function getBalance()
    {
        return $this->hasMany(StockAccountBalance::className(), ['account_id' => 'id'])->with('coinmarketcap');
    }



}
