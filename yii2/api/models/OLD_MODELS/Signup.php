<?php
namespace api\models;
use yii\db\ActiveRecord;
// use yii\db\Expression;
// use yii\behaviors\TimestampBehavior;
use api\models\Token;

class Signup extends ActiveRecord
{

    public static function tableName()
    {
        return 'user';
    }

    // public function behaviors()
    // {
    //     return [
    //         TimestampBehavior::className(),
    //     ];
    // }

    public function attributeLabels() {
        return [
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',

            'language_id' => 'language_id',
            'status' => 'status',
            'created_at' => 'created',
            'updated_at' => 'updated',
        ];
    }

    public function rules() {
        return [
            [['email','name'], 'trim'],
            [ ['name', 'email', 'password', 'language_id'], 'required' ],
            [ ['language_id', 'status', 'created_at', 'updated_at'], 'integer' ],
            ['name', 'string'],
            ['email', 'unique'],
            ['email', 'email', 'message' => "The email isn't correct"],
            ['password', 'string'],
        ];
    }



    /**
     * @return Token|null
     */
    public function auth($user_id)
    {
        $token = new Token();
        $token->user_id = $user_id;
        $token->generateToken(time() + 3600 * 24);
        return $token->save() ? $token : null;
    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    // protected function getUser()
    // {
    //     if ($this->_user === null) {
    //         $this->_user = Users::findByUsername($this->username);
    //     }
    //
    //     return $this->_user;
    // }

}
