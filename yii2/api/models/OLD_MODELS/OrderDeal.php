<?php
namespace api\models;
use yii\db\ActiveRecord;


class OrderDeal extends ActiveRecord
{
    public static function tableName()
    {
        return 'order_deal';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        //     'user_id' => 'user_id',
        //     'name' => 'name',
        //     'hold' => 'hold',
        //     'shared' => 'shared',
        //     'deleted' => 'deleted',
        //     'created_at' => 'created_at',
        //     'updated_at' => 'updated_at',
        // ];
    }

    public function rules() {
        return [
            [ ['pair_id'], 'required' ],
            // [ ['id', 'user_id', 'hold', 'shared', 'created_at', 'updated_at'], 'integer' ],
            // [['name'], 'trim'],
            // [['name'], 'string', 'max' => 30],

            // [['email','name'], 'trim'],
            // [ ['name', 'email', 'password', 'language_id'], 'required' ],
            // [ ['language_id', 'status', 'created_at', 'updated_at'], 'integer' ],
            // ['name', 'string'],
            // ['email', 'unique'],
            // ['email', 'email'],
            // ['password', 'string'],
        ];
    }

    public function getPair()
    {
        return $this->hasOne(Pair::className(), ['id' => 'pair_id']);
    }

    public function getAccount()
    {
        return $this->hasOne(StockAccount::className(), ['id' => 'stock_account_id'])->with(['stock']);
    }

    public function getStrategy()
    {
        return $this->hasOne(OrderStrategy::className(), ['id' => 'strategy_id']);
    }

    public function getCase()
    {
        return $this->hasOne(OrderDealHasCase::className(), ['deal_id' => 'id'])->with(['case']);
    }
    public function getTransaction()
    {
        return $this->hasMany(OrderTransaction::className(), ['deal_id' => 'id'])->with(['coin','account_from','account_to']);
    }


    //
    // public function getOwner()
    // {
    //     // $playlist_id = $this::findOne($this->item_id)->playlist_id;
    //     // $owner_id = Playlists::findOne()->user_id;
    //     return $this->user_id;
    // }
    // public function setActive($category_id)
    // {
    //     // return $this->hasOne(PlaylistsItems::className(), ['playlist_id' => 'id']);
    //     $this::findOne($category_id)->active = true;
    // }


    // public function getUser()
    // {
    //     return $this->hasOne(Users::className(), ['id' => 'user_id']);
    // }

}
