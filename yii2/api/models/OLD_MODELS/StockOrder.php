<?php
namespace api\models;
use yii\db\ActiveRecord;


class StockOrder extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock_order';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }
    public function getPair()
    {
        return $this->hasOne(Pair::className(), ['id' => 'pair_id']);
    }

    // public function getPair_usdt()
    // {
    //     return $this->hasMany(Pair::className(), ['id' => 'pair_id'])->where(['coin_to' => 'USDT']);
    // }

}
