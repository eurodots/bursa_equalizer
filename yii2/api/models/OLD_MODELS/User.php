<?php
namespace api\models;
use yii\db\ActiveRecord;


class User extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    public function getLanguage()
    {
        return $this->hasOne(UserLanguage::className(), ['id' => 'language_id']);
    }

}
