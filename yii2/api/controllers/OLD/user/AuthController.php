<?php

namespace api\controllers\user;

use Yii;
// use yii\filters\AccessControl;
use yii\web\Controller;
// use yii\web\Response;
// use yii\filters\VerbFilter;


use api\models\Signup;
use api\models\User;
use api\models\UserLanguage;
use api\models\Token;



header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Requested-With, Accept, X-Auth-Token , Authorization');
header('Access-Control-Max-Age: 1000');


class AuthController extends Controller
{

    public static function allowedDomains()
    {
        return [
            // '*',                        // star allows all domains
            'http://localhost:3000',
            // 'http://test2.example.com',
        ];
    }



    public function behaviors()
        {
            return array_merge(parent::behaviors(), [

                // For cross-domain AJAX request
                'corsFilter'  => [
                    'class' => \yii\filters\Cors::className(),
                    'cors'  => [
                        // restrict access to domains:
                        'Origin'                           => static::allowedDomains(),
                        'Access-Control-Request-Method'    => ['POST'],
                        'Access-Control-Allow-Credentials' => true,
                        'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)

                    ],
                ],

            ]);
    }

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */





    public function actionIndex($action=false)
    {

        echo "ok";

        exit();

    }


    public function actionSignin($action=false)
    {


        $request = getRequest('signin');

        if(
            isset($request['email']) &&
            isset($request['password'])
        ) {
            $email = $request['email'];
            $password = $request['password'];


            $errors_arr = [];

            // VALIDATE EMAIL
            $email_validator = new \yii\validators\EmailValidator();
            if (!$email_validator->validate($email, $error)) {
                $arr_type = 'email';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => $error,
                ];
                jsonOutput(['errors' => $errors_arr], 300, 'Errors');
            }


            // VALIDATE PASSWORD
            if(strlen($password) < 6 || strlen($password) > 20) {
                $arr_type = 'password';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => 'Password must be between 6 and 16 characters long',
                ];
                jsonOutput(['errors' => $errors_arr], 300, 'Errors');
            }

            // AUTH
            $query = User::find()->where(['email' => $email])->one();
            $user_id = $query ? $query->id : false;
            if($user_id == false) {
                $arr_type = 'email';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => 'Email not found',
                ];
                jsonOutput(['errors' => $errors_arr], 300, 'Errors');
            }

            $passwordCheck = Yii::$app->security->validatePassword($password, $query->password);
            if($passwordCheck == 1) {
                $model = new Signup();
                jsonOutput([
                    'response' => [
                        'token' => $model->auth($user_id)->token,
                    ],
                ], 200, 'Success!');
                exit();
            } else {
                $arr_type = 'password';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => 'Incorrect password',
                ];
                jsonOutput(['errors' => $errors_arr], 300, 'Errors');
            }

            exit();

        }
    }



    public function actionSignup($action=false)
    {

        $request = getRequest('signup');

        if(
            isset($request['name']) &&
            isset($request['email']) &&
            isset($request['password']) &&
            isset($request['captcha'])
        ) {
            $name = $request['name'];
            $email = $request['email'];
            $password = $request['password'];
            $captcha = $request['captcha'];
            $language = isset($request['language']) ? $request['language'] : false;


            $errors_arr = [];

            // VALIDATE EMAIL
            $email_validator = new \yii\validators\EmailValidator();
            if (!$email_validator->validate($email, $error)) {
                $arr_type = 'email';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => $error,
                ];
            }

            // VALIDATE NAME
            if(strlen($name) < 5 || strlen($name) > 20) {
                $arr_type = 'name';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => 'Name must be between 5 and 20 characters',
                ];
            }

            // VALIDATE PASSWORD
            if(strlen($password) < 6 || strlen($password) > 20) {
                $arr_type = 'password';
                $errors_arr[$arr_type] = [
                    'type' => $arr_type,
                    'error' => 'Password must be between 6 and 16 characters long',
                ];
            }



            if(count($errors_arr) > 0) {
                jsonOutput(['errors' => $errors_arr], 300, 'Errors');
            } else {


                // CHECK EMAIL EXIST
                $checkEmail = User::find()->where(['email' => $email])->one();
                if($checkEmail) {
                    $arr_type = 'email';
                    $errors_arr[$arr_type] = [
                        'type' => $arr_type,
                        'error' => 'Email already exist!',
                    ];
                    jsonOutput(['errors' => $errors_arr], 300, 'Errors');
                }



                // CHECK LANGUAGE
                $language_id = false;
                if($language) {

                    // default language
                    if(strlen($language) != 2) { $language = 'en'; }

                    $modelLanguage = UserLanguage::find()->where(['iso' => $language])->one();
                    if(count($modelLanguage) == 0) {
                        $model = new UserLanguage();
                        $model->iso = $language;
                        $model->name = $language;
                        $model->save();

                        $language_id = $model->id;
                    } else {
                        $language_id = $modelLanguage->id;
                    }
                }
                // print_r($language_id);



                // CHECK CAPTCHA
                if(strlen($captcha) == 0) {
                    $arr_type = 'captcha';
                    $errors_arr[$arr_type] = [
                        'type' => $arr_type,
                        'error' => 'Captcha not deteced',
                    ];
                    jsonOutput(['errors' => $errors_arr], 300, 'Errors');
                }

                $captcha_access = checkReCaptcha($captcha);
                if($captcha_access[0] == 0) {
                    $arr_type = 'captcha';
                    $errors_arr[$arr_type] = [
                        'type' => $arr_type,
                        'error' => $captcha_access[1],
                    ];
                    jsonOutput(['errors' => $errors_arr], 300, 'Errors');
                }


                // REGISTRATION
                $model = new Signup();
                $model->name = $name;
                $model->email = $email;
                $model->password = \Yii::$app->security->generatePasswordHash($password);
                $model->language_id = $language_id;
                $model->status = 0;
                $model->created_at = time();
                $model->updated_at = time();
                $model->save();

                $user_id = $model->id;


                jsonOutput([
                    'response' => [
                        'user_id' => $user_id,
                        'token' => $model->auth($user_id)->token,
                    ],
                ], 200, 'Success!');


            }



        } else {
            jsonOutput(false, 300, 'Not all data');
        }



        exit();

    }

}




function checkReCaptcha($captcha=false) {
    if($captcha) {
        $key = \Yii::$app->params['reCaptchaKey'];
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$key.'&response='.$captcha;
        $response = file_get_contents($url);
        $response = json_decode($response, TRUE);

        $response_status = isset($response['success']) ? ( $response['success'] ? 1 : 0) : 0;
        $response_msg = isset($response['error-codes'][0]) ? $response['error-codes'][0] : '';

        return [$response_status, $response_msg];

    }
}

function getRequest($logfile=false) {
    if($logfile) {
        $array = file_get_contents("php://input");
        $route = 'logs/'.$logfile.'.json';
        $fp = fopen($route, 'w');
        fwrite($fp, print_r($array, TRUE));
        fclose($fp);

        // $json = preg_replace( "/\p{Cc}*$/u", "", $array);
        return json_decode($array, TRUE);

    }
}

function jsonOutput($arrays=false, $status=200, $message=false) {
    $json = [
        "status" => $status,
        "message" => $message,
    ];
    if($arrays) {
        foreach ($arrays as $key => $arr) {
            $json[$key] = $arr;
        }
    }

    // print_r($json);
    print_r( json_encode($json) );
    exit();
}
