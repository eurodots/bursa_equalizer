<?php

namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use Larislackers\BinanceApi;
// use Larislackers\BinanceApiContainer;
// use Larislackers\BinanceApi\BinanceApiContainer;
use Binance\API;
use liqui\api\LiquiAPITrading;

use api\models\Coin;
use api\models\StockAccount;
use api\models\StockAccountBalance;
use api\models\StockOrder;
use api\models\OrderDeal;
// use yii\db\Expression;


// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en

header('Access-Control-Allow-Origin: *');

class ApistockController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */

     public function dealsChangeStatus($status=false, $api_order_id=false)
     {
         // $api_order_id = 7299201;

         if($status && $api_order_id) {

             $modelDeals = OrderDeal::find()->where(['api_order_id' => $api_order_id])->all();
             $counter = count($modelDeals);
             if($counter == 1) {

                 $modelDeals[0]->status = $status;
                 $modelDeals[0]->save();

                 return true;
             } elseif($counter > 1) {
                 print_r('FOUNDED DUPLICATED ORDERS!');
                 exit();
             }
         }

         exit();
     }

     public function getDealsId($stock_id=false)
     {
         if($stock_id) {
             $modelDeals = OrderDeal::find()->with(['pair','account'])->where(['status' => 0])->asArray()->all();
             $stockDeals = [];
             foreach ($modelDeals as $deal) {
                 if($stock_id == $deal['account']['stock_id'] && strlen($deal['api_order_id']) > 0) {
                     $stockDeals[] = [
                         "pair" => $deal['pair'],
                         "api_order_id" => $deal['api_order_id'],
                     ];
                 }
             }
             return $stockDeals;
         }

         return false;
     }

     public function actionIndex($action=false)
     {

         // $lastOrdersDate = \Yii::$app->params['lastOrdersDate'];
         // $lastOrdersDate = '-1 hour';
         // $modelOrders = StockOrder::find()->where(['>', 'created_at', $lastOrdersDate])->andWhere(['stock_id' => 3, 'pair_label' => 'XID_USDT'])->asArray()->all();
         // foreach ($modelOrders as $key => $order) {
         //     print_r([dateConverter($order['created_at'])]);
         //     print_r($order);
         // }
         // // print_r($modelOrders);
         // // echo "ok";
         // exit();

         // BinanceApiContainer $client = new Client(['verify' => false, 'http_errors' => false]);
         // "ssl"=> [
         //     "verify_peer"=>false,
         //     "verify_peer_name"=>false,
         // ],
         $modelAccount = StockAccount::find()->with(['stock'])->asArray()->all();
         // print_r($modelAccount);
         // exit();

         $apiKeys = [];
         foreach ($modelAccount as $account) {
             if(strlen($account['stock_api']) > 0) {
                 $api_string = explode(PHP_EOL,$account['stock_api']);
                 $stock_api = [];
                 foreach ($api_string as $api) {
                     $string_split = explode('=',$api);
                     $stock_api[$string_split[0]] = $string_split[1];
                 }
                 // print_r($stock_api);
                 $apiKeys[] = [
                     "account_id" => $account['id'],
                     "user_id" => $account['user_id'],
                     "stock_id" => $account['stock_id'],
                     "stock_api" => $stock_api,
                 ];
             }
         }
         // print_r($apiKeys);
         // exit();

         foreach ($apiKeys as $key => $api) {

             // LIQUI
             if($api['stock_id']==3) {
                 $accound_id = $api['account_id'];
                 $apiKey = isset($api['stock_api']['apiKey']) ? $api['stock_api']['apiKey'] : false;
                 $apiSecret = isset($api['stock_api']['apiSecret']) ? $api['stock_api']['apiSecret'] : false;
                 $apiVersion = 3;
                 if($apiKey && $apiSecret) {
                     $liqui = new LiquiAPITrading($apiKey, $apiSecret, $apiVersion);
                     $wallets = $liqui->getInfo();
                     foreach ($wallets['return']['funds'] as $wallet => $price) {
                         $coin_label = strtoupper($wallet);

                         // if($coin_label == 'BTC') {
                         //     print_r('btc');
                         //     print_r($price);
                         //     exit();
                         // }

                         $modelBalance = StockAccountBalance::find()->where(['coin_label' => $coin_label, 'account_id' => $accound_id])->one();
                         // print_r($modelBalance);
                         // exit();
                         if(count($modelBalance) > 0) {
                             $modelBalance->available = $price;
                             $modelBalance->on_order = 0;
                             $modelBalance->updated_at = time();
                             $modelBalance->save();
                             $modelBalance = [];
                         }
                     }

                 }
                 // exit();
             }

             // BINANCE
             if($api['stock_id']==2) {
                 $accound_id = $api['account_id'];
                 $apiKey = isset($api['stock_api']['apiKey']) ? $api['stock_api']['apiKey'] : false;
                 $apiSecret = isset($api['stock_api']['apiSecret']) ? $api['stock_api']['apiSecret'] : false;

                 if($apiKey && $apiSecret) {
                     $binance = new API($apiKey,$apiSecret);


                     // $test = $binance->orders("XRPETH");
                     // print_r($test);
                     // exit();


                     // GET ORDERS BY ID's
                     $stock_orders = $this->getDealsId(2);
                     if($stock_orders) {
                         $orders_from_stock = [];
                         foreach ($stock_orders as $order) {
                             $pair = $order['pair']['coin_from'].$order['pair']['coin_to'];
                             $order_id = $order['api_order_id'];
                             $orders_from_stock[] = $binance->orderStatus($pair, $order_id);
                         }

                         foreach ($orders_from_stock as $order) {
                             if(isset($order['status']) && $order['status'] == 'FILLED') {
                                 $response = $this->dealsChangeStatus(1, $order['orderId']);
                             }
                         }

                     }



                     // GET WALLETS BALANCE
                     $ticker = $binance->prices();
                     $wallets = $binance->balances($ticker);

                     foreach ($wallets as $key => $wallet) {

                         $wallet_available = $wallet['available'] ? $wallet['available'] : 0;
                         $wallet_on_order = $wallet['onOrder'] ? $wallet['onOrder'] : 0;


                         $modelBalance = StockAccountBalance::find()->where(['coin_label' => $key, 'account_id' => $accound_id])->one();
                         if(count($modelBalance) > 0) {
                             $modelBalance->available = $wallet_available;
                             $modelBalance->on_order = $wallet_on_order;
                             $modelBalance->updated_at = time();
                             $modelBalance->save();
                             $modelBalance = [];
                         } else {
                             // $modelBalance = new StockAccountBalance();
                             // $modelCoin = Coin::find()->where(['iso' => $key])->one();
                             // if(count($modelCoin)>0) {
                             //     $modelBalance->coin_id = $modelCoin->id;
                             //     $modelBalance->account_id = $accound_id;
                             //     $modelBalance->coin_label = $key;
                             //     $modelBalance->available = $wallet_available;
                             //     $modelBalance->on_order = $wallet_on_order;
                             //     $modelBalance->updated_at = time();
                             //     $modelBalance->save();
                             // }
                         }


                     }
                     // print_r($balances);
                 }


             }



         }


        // $ticker = $api->prices();

        // print_r($ticker); // List prices of all symbols
        // echo "Price of BNB: {$ticker['BNBBTC']} BTC.".PHP_EOL;


        // echo "BTC owned: ".$balances['BTC']['available'].PHP_EOL;
        // echo "ETH owned: ".$balances['ETH']['available'].PHP_EOL;
        // echo "Estimated Value: ".$api->btc_value." BTC".PHP_EOL;


        // $price = 0.00957401; //eth
        // $quantity = 10.00000000; //xrp
        // $icebergQty = 10;
        // $stopPrice = 09.00000000; // Sell immediately if price goes below 0.4 btc
        // $order = $api->sell("XRPETH", $quantity, $price);
        // $order = $api->sell("XRPETH", $quantity, $price, "LIMIT", ["stopPrice"=>$stopPrice]);

        // $order = $api->sell("XRPETH", $quantity, $price, "LIMIT", ["icebergQty"=>$icebergQty]);

        // print_r($order);

        // $history = $api->history("XRPETH");
        // print_r($history);


         // $bac = new BinanceApiContainer($apiKey, $apiSecret);

         // $time = time();
         // $serverTime =

        // $t = new \DateTime();
        // $t->setTimestamp($time);
        // $t->modify("-10 hours");
        // $serverTime = $t->getTimestamp()*1000;

         // echo $serverTime;
         // $orders = $bac->getOrderBook(['symbol' => 'BNBBTC']);
         // $orders = $bac->postOrder([
         //     'symbol' => 'BNBBTC',
         //     'side' => 'BUY',
         //     'type' => 'LIMIT',
         //     'quantity' => 1,
         //     'timestamp' => time(),
         //     'timeInForce' => 'GTC',
         //     'recvWindow' => 6000000,
         // ]);

         // *      @option string "symbol"           The symbol to search for. (required)
         // *      @option string "side"             Order side enum. (required)
         // *      @option string "type"             Order type enum. (required)
         // *      @option string "timeInForce"      Time in force enum. (required)
         // *      @option double "quantity"         Desired quantity. (required)
         // *      @option double "price"            Asking price. (required)
         // *      @option string "newClientOrderId" A unique id for the order. Automatically generated by default.
         // *      @option double "stopPrice"        Used with STOP orders.
         // *      @option double "icebergQty"       Used with icebergOrders.
         // *      @option int    "timestamp"        A UNIX timestamp. (required)

         // $response = $orders->getBody()->getContents();

        // X-MBX-APIKEY
        // hpk4qZnDsMJ8hOH57KdVZhccjRHoEsAdP04EPKMncVHJ8eF22TGscLX9BYDjlg06
        // https://api.binance.com/api/v3/order?symbol=LTCBTC&side=BUY&type=LIMIT&timeInForce=GTC&quantity=1&price=0.1&recvWindow=5000&timestamp=1507599597000&signature=c2e05141c5515e0e32aa0971b9ca61384b44dcbe0adb35fd906b90ae1383d3b4
        // print_r( json_encode($response) );
        // exit();
        exit();

     }


}
