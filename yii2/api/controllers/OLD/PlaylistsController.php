<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use api\models\Signup;
// use api\models\Users;
// use api\models\Language;
// use api\models\Token;

// use yii\db\Expression;
use api\models\Token;
use api\models\Playlists;
use api\models\PlaylistsItems;
use api\models\PlaylistsItemsParts;

// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en
// http://demohost.com:8888/signup?email=maaffa@adffdil.cfom&password=123&language=en
// http://demohost.com:8888/login?email=maaffa@fdil.cfom&password=123
// http://demohost.com:8888/token/KFwvIADhmDY7C06y3_77vSi9BgQMcc7J





header('Access-Control-Allow-Origin: *');



function getHomeUrl() {

    return Url::home(true);
    // return Url::home('https');

}

function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


class PlaylistsController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }


    public function ssave($runValidation=true,$attributes=null) {
        if(!static::save($runValidation,$attributes)) {
            throw new CDbException($this->errorSummary());
        }
        return true;
    }



    /**
     * Displays JSON videos.
     *
     * @return string
     */

    public function actionIndex($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);


            $query = Playlists::find()->with(['items'])->where(['user_id' => $user_id])->limit(100)->all();

            // print_r($query2);
            // exit();

            $arr = (object)[];
            $arr = array(
                'status' => 200,
                'user_id' => $user_id,
                'playlists' => [],
                // '_meta' => array(
                //     'playlist_add' => array(
                //         'url' => '/playlists/'.$token.'/playlist_add?',
                //         'params' => '?name={string}&shared={0,1}',
                //     ),
                //     'search_item_by_video_id' => array(
                //         'url' => '/playlists/'.$token.'/search_item_by_video_id/',
                //         'params' => '.../{video_id}',
                //     ),
                //     // 'reload' => array(
                //     //     'url' => Url::home(true).'playlists/'.$token,
                //     // ),
                //     // 'playlists_count' => count($query),
                //
                // ),
            );

            // Playlists
            foreach ($query as $key_q => $q) {
                if($q->deleted==0) {
                    $arr['playlists'][] = array(
                        'manual' => false,
                        'id' => $q->id,
                        'name' => $q->name,
                        'active' => $q->active,
                        'hold' => $q->hold,
                        'shared' => $q->shared,
                        'deleted' => $q->deleted,
                        // 'subcaption' => $q->hold ? 'Default' : time_elapsed_string('@'.$q->created_at),
                        'updated' => $q->updated_at,
                        // 'created_at' => $q->created_at,
                        // 'updated_at' => $q->updated_at,
                        'items' => [],
                        // '_meta' => array (
                        //     'add_item_and_part' => array(
                        //         'url' => getHomeUrl().'playlists/'.$token.'/add_item_and_part?playlist_id='.$q->id,
                        //         'params' => '&video_id={int}&name={string}&start={int}&end={int}'
                        //     ),
                        //     'hold' => array(
                        //         'url' => '/playlists/'.$token.'/playlist_hold?playlist_id='.$q->id,
                        //         'params' => '&hold={0,1}'
                        //     ),
                        //     'edit' => array(
                        //         'url' => '/playlists/'.$token.'/playlist_edit?playlist_id='.$q->id,
                        //         'params' => '&name={string}&shared={0,1}',
                        //     ),
                        //     'delete' => array(
                        //         'url' => '/playlists/'.$token.'/playlist_delete?playlist_id='.$q->id,
                        //         'params' => '&delete={0,1}',
                        //     ),
                        //     'item_add' => array(
                        //         'url' => '/playlists/'.$token.'/item_add?playlist_id='.$q->id,
                        //         'params' => '&video_id={int}&name={string}'
                        //     ),
                        //     // 'items_count' => count($q->items),
                        // )
                    );
                    $playlist_id = count($arr['playlists'])-1;

                    // Items
                    foreach ($q->items as $key_i => $item) {
                        if($item->deleted==0) {
                            // print_r($item);
                            $arr['playlists'][$playlist_id]['items'][] = array(
                                'manual' => false,
                                'id' => $item->id,
                                'playlist_id' => $item->playlist_id,
                                'video_id' => $item->video_id,
                                'video_identity' => $item->video_identity,
                                'name' => $item->name,
                                'thumbnail' => getHomeUrl().'../video/image?v='.$item->video_id,
                                'status' => $item->status,
                                'deleted' => $item->deleted,
                                // 'created_at' => $item->created_at,
                                // 'updated_at' => $item->updated_at,
                                'parts' => [],
                                // '_meta' => array (
                                //     'delete' => array(
                                //         'url' => '/playlists/'.$token.'/item_delete?item_id='.$item->id,
                                //         'params' => '?delete={0,1}',
                                //     ),
                                //     'change_playlist' => array(
                                //         'url' => '/playlists/'.$token.'/item_change_playlist?item_id='.$item->id,
                                //         'params' => '?playlist_id={int}',
                                //     ),
                                //     'part_add' => array(
                                //         'url' => getHomeUrl().'playlists/'.$token.'/part_add?item_id='.$item->id,
                                //         'params' => '?name={string}&start={int}&end={int}',
                                //     ),
                                //     // 'parts_count' => false,
                                // )
                            );

                            $item_id = count($arr['playlists'][$playlist_id]['items'])-1;

                            // Parts
                            $parts = PlaylistsItemsParts::find()->with(['item'])->where(['item_id' => $item->id])->limit(100)->all();
                            // $arr['playlists'][$key_q]['items'][$key_i]['_meta']['parts_count'] = count($parts);
                            foreach ($parts as $key_p => $part) {
                                if($part->deleted == 0) {
                                    $arr['playlists'][$playlist_id]['items'][$item_id]['parts'][] = array(
                                        'manual' => false,
                                        'id' => $part->id,
                                        'item_id' => $part->item_id,
                                        'start' => $part->start,
                                        'end' => $part->end,
                                        'name' => $part->name,
                                        'deleted' => $part->deleted,
                                        // 'deleted' => $part->deleted,
                                        // 'created_at' => $part->created_at,
                                        // 'updated_at' => $part->updated_at,
                                        // '_meta' => array(
                                        //     'change' => array(
                                        //         'url' => '/playlists/'.$token.'/part_change?part_id='.$part->id,
                                        //         'params' => '?name={string}&start={int}&end={int}',
                                        //     ),
                                        //     'delete' => array(
                                        //         'url' => '/playlists/'.$token.'/part_delete?part_id='.$part->id,
                                        //         'params' => '?delete={0,1}',
                                        //     ),
                                        // ),
                                    );
                                }
                            }

                        }
                    }
                }

            }

            print_r( json_encode($arr) );

        } else {
            echo "Token wrong";
            exit();
        }
    }


    public function actionPlaylist_add($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            // echo $user_id;
            if(
                $user_id > 0 &&
                isset($_GET['name']) &&
                isset($_GET['shared'])
            ) {

                $name = $_GET['name'];
                $shared = (int)$_GET['shared'];

                $model = new Playlists();
                $model->user_id = $user_id;
                $model->name = $name;
                $model->hold = 0;
                $model->shared = $shared;
                $model->deleted = 0;
                $model->created_at = time();
                $model->updated_at = time();
                $model->save();

                // Set default active for all categories
                $model = Playlists::find()->where(['user_id' => $user_id])->all();
                foreach ($model as $key => $m) {
                    $m->active = false;
                    $m->save();
                }

                $respond = [
                    "status" => 200,
                    "message" => "Playlist added"
                ];
                print_r( json_encode($respond) );
                exit();

            } else {
                echo "Not all params";
                exit();
            }

        } else {
            echo "Token wrong";
            exit();
        }
    }


    public function actionPlaylist_hold($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['playlist_id']) &&
                isset($_GET['hold'])
            ) {
                $playlist_id = (int)$_GET['playlist_id'];
                $hold = (int)$_GET['hold'];

                if(
                    Playlists::findOne($playlist_id) &&
                    Playlists::findOne($playlist_id)->owner == $user_id
                ) {
                    $model = Playlists::find()->where(['id' => $playlist_id, 'user_id' => $user_id])->one();
                    $model->hold = $hold;
                    $model->updated_at = time();
                    $model->save();

                    echo "Done!";
                } else {
                    echo "Playlist: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionPlaylist_edit($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['playlist_id']) &&
                isset($_GET['name']) &&
                isset($_GET['shared'])
            ) {
                $playlist_id = (int)$_GET['playlist_id'];
                $name = (string)$_GET['name'];
                $shared = (int)$_GET['shared'];

                if(
                    Playlists::findOne($playlist_id) &&
                    Playlists::findOne($playlist_id)->owner == $user_id
                ) {
                    $model = Playlists::find()->where(['id' => $playlist_id, 'user_id' => $user_id])->one();
                    $model->name = mb_substr($name, 0, 10);
                    $model->shared = $shared;
                    $model->updated_at = time();
                    $model->save();

                    $respond = [
                        "status" => 200,
                        "message" => "Playlist changed!"
                    ];
                    print_r( json_encode($respond) );
                    exit();

                } else {
                    echo "Playlist: wrong owner or not exist";
                    exit();
                }
            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionPlaylist_delete($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['playlist_id']) &&
                isset($_GET['delete'])
            ) {
                $playlist_id = (int)$_GET['playlist_id'];
                $delete = (int)$_GET['delete'];

                if(
                    Playlists::findOne($playlist_id) &&
                    Playlists::findOne($playlist_id)->owner == $user_id
                ) {
                    $model = Playlists::find()->where(['id' => $playlist_id, 'user_id' => $user_id])->one();
                    $model->deleted = $delete;
                    $model->updated_at = time();
                    $model->save();

                    $respond = [
                        "status" => 200,
                        "message" => "Playlist deleted"
                    ];
                    print_r( json_encode($respond) );
                    exit();

                } else {
                    echo "Playlist: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionSearch_item_by_video_id($token=false, $video_id=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                $video_id != false
            ) {
                $video_id = (int)$video_id;

                $getPlaylists = Playlists::find()->with(['items'])->where(['user_id' => $user_id, 'deleted' => 0])->all();
                if(count($getPlaylists)>0) {
                    $foundedItem = false;
                    foreach ($getPlaylists as $key => $playlist) {
                        foreach ($playlist->items as $k => $item) {
                            if($item->video_id == $video_id) {
                                $foundedItem = $item;
                                break;
                            }
                        }
                    }
                    // print_r($foundedItem);
                    if($foundedItem != false) {
                        $respond = [
                            "status" => 200,
                            "message" => "Founded",
                            "playlist_id" => $foundedItem->playlist_id,
                            "item_id" => $foundedItem->id,
                            "video_id" => $video_id
                        ];
                        print_r( json_encode($respond) );
                        exit();
                    } else {
                        $respond = [
                            "status" => 400,
                            "message" => "Not found",
                        ];
                        print_r( json_encode($respond) );
                        exit();
                    }

                } else {
                    echo "Error";
                    exit();
                }

                // if(count($getItem)>0) {
                //     foreach ($getItem as $key => $item) {
                //         $getPlaylistId = $item->playlist_id;
                //
                //     }
                //     // $getPlaylist
                // } else {
                //     // video_id not found
                // }
                // print_r($getItem);
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionAdd_item_and_part($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['video_id']) &&
                isset($_GET['name']) &&
                isset($_GET['start']) &&
                isset($_GET['end'])
            ) {
                $video_id = (int)$_GET['video_id'];
                $name = (string)$_GET['name'];
                $start = (int)$_GET['start'];
                $end = (int)$_GET['end'];

                $getFirstPlaylist = Playlists::find()->where(['user_id' => $user_id, 'deleted' => 0])->one();



                if($getFirstPlaylist) {

                    $model = Playlists::findOne($getFirstPlaylist->id);
                    $model->active = true;
                    $model->save();

                    $model = new PlaylistsItems();
                    $model->playlist_id = $getFirstPlaylist->id;
                    $model->video_id = $video_id;
                    $model->name = $name;
                    $model->status = 0;
                    $model->deleted = 0;
                    $model->created_at = time();
                    $model->updated_at = time();
                    $model->save();

                    $item_id = $model->id;

                    $model = new PlaylistsItemsParts();
                    $model->item_id = $item_id;
                    $model->start = $start;
                    $model->end = $end;
                    $model->name = $name;
                    $model->deleted = 0;
                    $model->created_at = time();
                    $model->updated_at = time();
                    $model->save();

                    $respond = [
                        "status" => 200,
                        "message" => "Item and Part added!"
                    ];
                    print_r( json_encode($respond) );
                    exit();
                }

            }
        }
    }

    public function actionItem_add($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['playlist_id']) &&
                isset($_GET['video_id']) &&
                isset($_GET['name'])
            ) {
                $playlist_id = (int)$_GET['playlist_id'];
                $video_id = (int)$_GET['video_id'];
                $name = (string)$_GET['name'];

                if(
                    // PlaylistsItems::findOne($video_id) &&
                    // PlaylistsItems::findOne($video_id)->owner == $user_id
                    Playlists::findOne($playlist_id) &&
                    Playlists::findOne($playlist_id)->owner == $user_id
                ) {

                    // Check if item alreasy added in some user active playlist
                    $items = PlaylistsItems::find()->where(['video_id' => $video_id, 'deleted' => 0])->all();
                    if(count($items) > 0) {

                        $playlists = Playlists::find()->where(['user_id' => $user_id, 'deleted' => 0])->all();

                        $activeItemsCounter = false;
                        foreach ($playlists as $playlist) {
                            foreach ($items as $item) {
                                if($playlist->id == $item->playlist_id) {
                                    $activeItemsCounter = $item;
                                }
                            }
                        }
                        if($activeItemsCounter != false) {

                            // Change playlist_id for item
                            $model = PlaylistsItems::findOne($activeItemsCounter->id);
                            $model->playlist_id = $playlist_id;
                            $model->updated_at = time();
                            $model->save();

                            // Set active category
                            $model = Playlists::find()->where(['user_id' => $user_id])->all();
                            foreach ($model as $key => $m) {
                                $m->active = false;
                                if($m->id == $playlist_id) {
                                    $m->active = true;
                                    $m->updated_at = time();
                                }
                                $m->save();
                            }

                            $respond = [
                                "status" => 200,
                                "message" => "Playlist changed!"
                            ];
                            print_r( json_encode($respond) );
                            exit();
                        }

                    }

                    $model = new PlaylistsItems();
                    $model->playlist_id = $playlist_id;
                    $model->video_id = $video_id;
                    $model->name = $name;
                    $model->status = 0;
                    $model->deleted = 0;
                    $model->created_at = time();
                    $model->updated_at = time();
                    $model->save();

                    // Set active category
                    $model = Playlists::find()->where(['user_id' => $user_id])->all();
                    foreach ($model as $key => $m) {
                        $m->active = false;
                        if($m->id == $playlist_id) {
                            $m->active = true;
                            $m->updated_at = time();
                        }
                        $m->save();
                    }

                    // $model = Playlists::findOne($playlist_id);
                    // $model->active = true;
                    // $model->updated_at = time();
                    // $model->save();

                    // Playlists::findOne($playlist_id)->active = true;
                    // print_r(Playlists::findOne($playlist_id));
                    // exit();

                    $respond = [
                        "status" => 200,
                        "message" => "Item added!"
                    ];
                    print_r( json_encode($respond) );
                    exit();

                } else {
                    echo "Playlist: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }

        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionItem_delete($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['item_id']) &&
                isset($_GET['delete'])
            ) {
                $item_id = (int)$_GET['item_id'];
                $delete = (int)$_GET['delete'];

                if(
                    PlaylistsItems::findOne($item_id) &&
                    PlaylistsItems::findOne($item_id)->owner == $user_id
                ) {
                    $model = PlaylistsItems::findOne($item_id);
                    $model->deleted = $delete;
                    $model->updated_at = time();
                    $model->save();

                    $respond = [
                        "status" => 200,
                        "message" => "Item deleted!"
                    ];
                    print_r( json_encode($respond) );
                    exit();

                } else {
                    echo "Item: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionItem_change_playlist($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['item_id']) &&
                isset($_GET['playlist_id'])
            ) {
                $item_id = (int)$_GET['item_id'];
                $playlist_id = (int)$_GET['playlist_id'];


                if(
                    PlaylistsItems::findOne($item_id) &&
                    PlaylistsItems::findOne($item_id)->owner == $user_id &&
                    Playlists::findOne($playlist_id) &&
                    Playlists::findOne($playlist_id)->owner == $user_id
                ) {
                    $model = PlaylistsItems::findOne($item_id);
                    $model->playlist_id = $playlist_id;
                    $model->updated_at = time();
                    $model->save();

                    echo "Done!";
                } else {
                    echo "Playlist or Item: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionPart_add($token=false)
    {

        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['item_id']) &&
                isset($_GET['name']) &&
                isset($_GET['start']) &&
                isset($_GET['end'])
            ) {
                $item_id = (int)$_GET['item_id'];
                $name = (string)$_GET['name'];
                $start = (int)$_GET['start'];
                $end = (int)$_GET['end'];


                if(
                    PlaylistsItems::findOne($item_id) &&
                    PlaylistsItems::findOne($item_id)->owner == $user_id
                ) {
                    $model = new PlaylistsItemsParts();
                    $model->item_id = $item_id;
                    $model->start = $start;
                    $model->end = $end;
                    $model->name = $name;
                    $model->deleted = 0;
                    $model->created_at = time();
                    $model->updated_at = time();
                    $model->save();

                    $respond = [
                        "status" => 200,
                        "message" => "Part added!"
                    ];
                    print_r( json_encode($respond) );
                    exit();

                } else {
                    echo "Item: wrong owner or not exist";
                    exit();
                }
            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionPart_change($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['part_id']) &&
                isset($_GET['name']) &&
                isset($_GET['start']) &&
                isset($_GET['end'])
            ) {
                $part_id = (int)$_GET['part_id'];
                $name = (string)$_GET['name'];
                $start = (int)$_GET['start'];
                $end = (int)$_GET['end'];

                if(
                    PlaylistsItemsParts::findOne($part_id) &&
                    PlaylistsItemsParts::findOne($part_id)->owner == $user_id
                ) {
                    $model = PlaylistsItemsParts::findOne($part_id);
                    $model->name = $name;
                    $model->start = $start;
                    $model->end = $end;
                    $model->updated_at = time();
                    $model->save();

                    echo "Done!";
                } else {
                    echo "Part: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }

    public function actionPart_delete($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if(
                $user_id > 0 &&
                isset($_GET['part_id']) &&
                isset($_GET['delete'])
            ) {
                $part_id = (int)$_GET['part_id'];
                $delete = (int)$_GET['delete'];

                if(
                    PlaylistsItemsParts::findOne($part_id) &&
                    PlaylistsItemsParts::findOne($part_id)->owner == $user_id
                ) {
                    $model = PlaylistsItemsParts::findOne($part_id);
                    $model->deleted = $delete;
                    $model->updated_at = time();
                    $model->save();

                    echo "Done!";
                } else {
                    echo "Part: wrong owner or not exist";
                    exit();
                }

            } else {
                echo "Not all params";
                exit();
            }
        } else {
            echo "Token wrong";
            exit();
        }
    }


}
