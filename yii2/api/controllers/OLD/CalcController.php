<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use api\models\Token;

use api\models\Pair;
use api\models\Stock;
// use api\models\StockPair;
use api\models\StockOrder;
use api\models\StockAccount;
use api\models\StockAccountBalance;
use api\models\StatCoinmarketcap;

use api\models\User;


header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');


class CalcController extends Controller
{

    // public $tmp_orders_sell = [];
    // public $tmp_orders_buy = [];
    // public $tmp_transactions = [];
    // public $tmp_group_counter = 0;

    /*
    public function combTransactions() {


        // print_r($this->tmp_orders_sell);
        // exit();

        $balance = 0;
        $stop = false;

        foreach ($this->tmp_orders_buy as $key_b => $buy) {
            // print_r($buy);
            // exit();

            foreach ($this->tmp_orders_sell as $key_s => $sell) {

                if($stop) { return false; }

                $price_sell = $sell['price'];
                $price_buy = $buy['price'];
                $total_sell = $sell['total'];
                $total_buy = $buy['total'];

                $buy_balance = $total_buy - $total_sell;

                $profit_sell = $total_sell-(($price_sell/$price_buy)*$total_sell);
                $profit_buy = $total_buy-(($price_sell/$price_buy)*$total_buy);


                // $profit_percent = ((($price_buy - $price_sell) / ($price_buy + $price_sell)) / 2) * 100;
                $profit_percent_sell = ($profit_sell * 100)/$total_sell;
                $profit_percent_buy = ($profit_buy * 100)/$total_buy;

                $history = [
                    "original" => $sell['original'],
                    "sell" => [
                        "coin_from" => $sell['data']['coin_from'],
                        "price" => $price_sell." ".$sell['data']['coin_to'],
                        "total" => $total_sell." ".$sell['data']['coin_to'],
                    ],
                    "buy" => [
                        "coin_to" => $sell['data']['coin_to'],
                        "price" => $price_buy." ".$sell['data']['coin_to'],
                        "total" => $total_buy." ".$sell['data']['coin_to'],
                    ],
                ];

                if($buy_balance >= 0) {
                    $this->tmp_orders_buy[$key_b]['total'] = $buy_balance;
                    unset($this->tmp_orders_sell[$key_s]);
                    $this->tmp_transactions[$this->tmp_group_counter][] = [
                        "volume" => $total_sell,
                        "profit_percent" => $profit_percent_sell,
                        "profit" => $profit_sell,
                        "total" => $total_sell * $price_buy,
                        "history" => $history,
                    ];
                    $stop = true;
                    $this->combTransactions();
                } else {
                    $this->tmp_orders_sell[$key_s]['total'] =- $buy_balance;
                    unset($this->tmp_orders_buy[$key_b]);
                    if($buy['total'] != 0) {
                        $this->tmp_transactions[$this->tmp_group_counter][] = [
                            "volume" => $total_buy,
                            "profit_percent" => $profit_percent_buy,
                            "profit" => $profit_buy,
                            "total" => $total_buy * $price_sell,
                            "history" => $history,
                        ];
                    }
                    $this->tmp_group_counter++;

                    $stop = true;
                    $this->combTransactions();
                }


            }
        }

        // print_r($this->tmp_transactions);
        // exit();

        return $this->tmp_transactions;
    }
    */

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */
     public function getBallanceForComb($user_id, $stock_id, $coin_to)
     {


         $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
         $modelAccounts = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0, 'stock_id' => $stock_id])->with(['balance','stock'])->asArray()->all();

         // print_r([$stock_id]);
         // print_r($modelAccounts);
         // exit();

         $arrBalance = [];
         foreach ($modelAccounts as $account) {

             $account_name = $account['name'];
             $account_id = $account['id'];

             $arrBalance[$account_id] = [
                 "account_id" => $account_id,
                 "account_name" => $account_name,
                 "commission" => $account['stock']['commission'],
                 // "account_total_usd" => false,
                 "balance" => [],
             ];
             foreach ($account['balance'] as $balance) {
                 if($balance['available'] > 0) {

                     $price_usd = false;
                     foreach ($modelCoinmarketcap as $coinmarketcap) {
                         if($balance['coin_label'] == $coinmarketcap['coin_label']) {
                             $price_usd = $coinmarketcap['price_usd'];
                         }
                     }
                     if($price_usd) {
                         $price_usd = $price_usd * $balance['available'];
                         $price_usd = round($price_usd, 2);
                     }

                     $orders = [];
                     $modelPairs = Pair::find()->where(['coin_from' => $balance['coin_label'], 'coin_to' => $coin_to])->asArray()->one();
                     // print_r($modelPairs);

                     if(count($modelPairs) > 0) {

                         $pair_id = $modelPairs['id'];
                         // print_r($modelPairs);

                         $date = \Yii::$app->params['lastOrdersDate'];
                         $modelOrders = StockOrder::find()->where(['>', 'created_at', $date])->andWhere(['stock_id' => $stock_id, 'pair_id' => $pair_id, 'type' => 1])->asArray()->all();

                         // print_r($modelOrders);

                         if(count($modelOrders) > 0) {

                             // print_r($modelOrders);
                             // exit();

                             usort($modelOrders,function($first,$second){
                                 return $first['price'] < $second['price'];
                             });
                             $modelOrders = array_slice($modelOrders, 0, 10);

                             foreach ($modelOrders as $order) {
                                 $orders[] = [
                                     "volume" => $order['volume'],
                                     "price" => $order['price'],
                                     "total" => $order['price'] * $order['volume'],
                                     "sum" => 0,
                                     "created_at" => dateConverter($order['created_at']),
                                 ];
                             }
                         }
                     }
                     // print_r($orders);
                     // exit();

                     if($price_usd >= 1) {
                         $arrBalance[$account_id]['balance'][] = [
                             "account_id" => $account_id,
                             "account_name" => $account_name,
                             "coin_label" => $balance['coin_label'],
                             "available" => $balance['available'],
                             "on_order" => $balance['on_order'],
                             "price_usd" => $price_usd,
                             "updated_at" => $balance['updated_at'],
                             "active" => false,
                             "volume_handle" => "0.00000000", // for react
                             "volume_handle_tmp" => 0, // for react
                             "coinmarketcap" => false,
                             "orders" => $orders,
                         ];
                     }

                 }
             }

         }

         foreach ($arrBalance as $key => $account) {
             usort($arrBalance[$key]['balance'],function($first,$second){
                 return $first['price_usd'] < $second['price_usd'];
             });
         }


         // APPLY COINMARKETCAP DATA
         $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
         foreach ($arrBalance as $key_acc => $account) {
             foreach ($account['balance'] as $key_b => $balance) {
                 foreach ($modelCoinmarketcap as $coinmarketcap) {
                     if($balance['coin_label'] == $coinmarketcap['coin_label']) {
                         $arrBalance[$key_acc]['balance'][$key_b]['coinmarketcap'] = [
                             "link" => "https://coinmarketcap.com/currencies/".$coinmarketcap['name']."/",
                             "coin_label" => $coinmarketcap['coin_label'],
                             "rank" => $coinmarketcap['rank'],
                             "percent_change_1h" => $coinmarketcap['percent_change_1h'],
                             "percent_change_24h" => $coinmarketcap['percent_change_24h'],
                             "percent_change_7d" => $coinmarketcap['percent_change_7d'],
                             "updated_at" => dateConverter($coinmarketcap['updated_at']),
                         ];
                     }
                 }

             }

         }


         return $arrBalance;
     }


    public function actionIndex($token=false, $stock_id=0, $coin_to=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $stock_id = isset($_GET['stock_id']) ? $_GET['stock_id'] : false;
                $coin_to = isset($_GET['coin_to']) ? $_GET['coin_to'] : false;



                $modelStock = Stock::find()->where(['id' => $stock_id])->asArray()->one();

                if(count($modelStock)>0 && $coin_to && $coin_to) {

                    // // GET COIN_TO FROM ALL PAIRS OF CURRENT STOCK_ID_SELL
                    // $modelStock_sell_pairs = StockPair::find()->where(['whitelist' => 1, 'stock_id' => $modelStock_sell])->with(['pair'])->asArray()->all();
                    // $arrCoinsTo = [];
                    // foreach ($modelStock_sell_pairs as $pair) {
                    //     $arrCoinsTo[] = $pair['pair']['coin_to'];
                    // }
                    // $arrCoinsTo = array_unique($arrCoinsTo);

                    $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
                    // print_r($modelCoinmarketcap);
                    // exit();
                    $modelStockAccounts = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0])->andWhere(['!=', 'stock_id', $stock_id])->with(['balance','stock'])->asArray()->all();
                    $walletsArr = [];
                    if(count($modelStockAccounts) > 0) {
                        foreach ($modelStockAccounts as $account) {
                            // print_r($account['stock']);
                            // exit();

                            $acc_stock_id = $account['stock']['id'];
                            $acc_stock_name = $account['stock']['name'];

                            $walletsArr[$acc_stock_name] = [
                                "stock_id" => $acc_stock_id,
                                "stock_name" => $acc_stock_name,
                                "stock_website" => 'https://'.$account['stock']['website'],
                                "commission" => $account['stock']['commission'],
                                "accounts" => [],
                            ];
                        }

                        foreach ($modelStockAccounts as $account) {
                            $acc_stock_id = $account['stock']['id'];
                            $acc_stock_name = $account['stock']['name'];

                            $account_id = $account['id'];
                            $account_name = $account['name'];


                            $walletsArr[$acc_stock_name]['accounts'][$account_name] = [
                                "account_id" => $account_id,
                                "account_name" => $account_name,
                                "account_total_usd" => false,
                                "balance" => [],
                            ];
                            // $walletsArr[$stock_id]['accounts'][$account_id]['account_name'] = $account_id;
                        }

                        // print_r($walletsArr);
                        // exit();

                        foreach ($modelStockAccounts as $account) {

                            $acc_stock_name = $account['stock']['name'];
                            $account_name = $account['name'];


                            $balanceArr = [];
                            foreach ($account['balance'] as $balance) {

                                if($balance['available'] > 0) {

                                    $price_usd = 0;
                                    foreach ($modelCoinmarketcap as $coinmarketcap) {
                                        if($coinmarketcap['coin_label'] == $balance['coin_label']) {
                                            $price_usd = $balance['available'] * $coinmarketcap['price_usd'];
                                        }
                                    }

                                    if($price_usd >= 1) {
                                        $balanceArr[] = [
                                            "account_id" => $balance['account_id'],
                                            "coin_label" => $balance['coin_label'],
                                            "available" => $balance['available'],
                                            "price_usd" => $price_usd,
                                            "updated_at" => dateConverter($balance['updated_at']),
                                        ];
                                    }

                                }
                            }
                            usort($balanceArr,function($first,$second){
                                return $first['price_usd'] < $second['price_usd'];
                            });

                            // print_r($stock);
                            // exit();

                            if(count($balanceArr)>0) {
                                $walletsArr[$acc_stock_name]['accounts'][$account_name]['balance'] = $balanceArr;

                                // $stock_id = $account['stock']['id'];
                                // $walletsArr[$stock_id] = [
                                //     "stock_id" => $account['stock']['id'],
                                //     "stock_name" => $account['stock']['name'],
                                //     "accounts" => $balanceArr,
                                // ];
                            }

                            // $walletsArr[$stock_name]['accounts'][] = $balanceArr;


                        }

                    }

                    $calculateWallets = [];
                    foreach ($walletsArr as $key_stock => $stock) {
                        // print_r($stock);
                        // exit();
                        foreach ($stock['accounts'] as $key_account => $account) {
                            $sum_price = 0;
                            foreach ($account['balance'] as $balance) {
                                $sum_price += $balance['price_usd'];
                            }
                            $walletsArr[$key_stock]['accounts'][$key_account]['account_total_usd'] = $sum_price;
                        }

                    }
                    // print_r($walletsArr);
                    // exit();

                    // print_r($stock_id);
                    // exit();


                    $arrBalance = [
                        // "stock_coins_to" => $arrCoinsTo,
                        "stock_website" => "https://".$modelStock['website'],
                        "stock_name" => $modelStock['name'],
                        "commission" => $modelStock['commission'],
                        "accounts" => $this->getBallanceForComb($user_id, $stock_id, $coin_to),
                    ];

                    $result = array (
                        "status" => 200,
                        "results" => $arrBalance,
                        "wallets" => $walletsArr,
                    );
                    print_r( json_encode($result) );
                    exit();
                }

            }
        }

        exit();
    }

    /*
    public function actionCalc_orders($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {


                $json = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($json, TRUE));
                fclose($fp);


                echo "OK";


            }
        }

        exit();
    }



    public function actionConvert_balance($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $json = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($json, TRUE));
                fclose($fp);

                // exit();
                // $json = preg_replace( "/\p{Cc}*$/u", "", $json);
                $json = json_decode($json, TRUE);
                $stock_id = $json['stock_id'];
                $balance = $json['balance'];
                $coin_to = $json['convert_to_coin'];

                // $coin_to = 'BTC';

                $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();

                if (is_array($balance) || is_object($balance)) {

                    $balanceGroup = [];
                    foreach ($balance as $item) {
                        // print_r($item);

                         $price_usd = 0;
                         $price_usd_coin_to = 0;

                         foreach ($modelCoinmarketcap as $coinmarketcap) {
                             if($coinmarketcap['coin_label'] == $item['coin_label']) {
                                 $price_usd = $item['available'] * $coinmarketcap['price_usd'];
                             }
                             if($coinmarketcap['coin_label'] == $coin_to) {
                                 $price_usd_coin_to = $coinmarketcap['price_usd'];
                             }
                         }

                         if($item['coin_label'] != $coin_to) {
                             $price = ($price_usd / $price_usd_coin_to) / $item['available'];
                             $group = $item['coin_label'];
                             $balanceGroup[$group][] = [
                                 "volume" => $item['available'],
                                 "price" => $price,
                                 "total" => $price * $item['available'],
                                 "data" => [
                                     "coin_from" => $item['coin_label'],
                                     "coin_to" => $coin_to,
                                 ],
                                 "original" => $item,
                             ];
                         }

                    }


                    $date = \Yii::$app->params['lastOrdersDate'];
                    $modelOrders = StockOrder::find()->where(['>', 'created_at', $date])->where(['stock_id' => $stock_id, 'type' => 1])->with(['pair'])->asArray()->all();

                    $transactionsByGroups = [];
                    if(count($modelOrders)>0) {

                        foreach ($balanceGroup as $key_coin_from => $orders_sell) {

                            $orders_buy = [];
                            foreach ($modelOrders as $order) {
                                if($order['pair']['coin_from'] == $key_coin_from && $order['pair']['coin_to'] == $coin_to) {

                                    // $price_usd = 0;
                                    // foreach ($modelCoinmarketcap as $coinmarketcap) {
                                    //     if($coinmarketcap['coin_label'] == $item['coin_label']) {
                                    //         $price_usd = $item['available'] * $coinmarketcap['price_usd'];
                                    //     }
                                    // }

                                    $orders_buy[] = [
                                        "volume" => $order['volume'],
                                        "price" => $order['price'],
                                        // "price_usd" => $price_usd,
                                        "total" => $order['volume'] * $order['price'],
                                    ];
                                }
                            }
                            usort($orders_buy,function($first,$second){
                                return $first['price'] < $second['price'];
                            });

                            $this->tmp_group_counter = 0;
                            $this->tmp_transactions = [];
                            $this->tmp_orders_sell = $orders_sell;
                            $this->tmp_orders_buy = $orders_buy;
                            $this->combTransactions();


                            // print_r([$key_coin_from."_".$coin_to]);
                            // print_r($orders_sell);
                            $transactionsByCoins[$key_coin_from] = $this->tmp_transactions;

                            // exit();

                        }
                    }

                    $resultsByAccounts = [];
                    // foreach ($transactionsByCoins as $key_coin_from => $group) {
                    //     print_r($group);
                    //     exit();
                    //     // $resultsByAccounts[] = [
                    //     //
                    //     // ]
                    // }
                    print_r($transactionsByCoins);
                    // print_r($resultsByAccounts);
                    exit();


                    $result = array (
                        "status" => 200,
                        "results" => $results,
                    );
                    print_r( json_encode($result) );
                    exit();

                }


            }
        }

        exit();
    }
    */


}


function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}

function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}
