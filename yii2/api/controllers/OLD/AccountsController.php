<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use api\models\Signup;
// use api\models\Users;
// use api\models\Language;
// use api\models\Token;

// use yii\db\Expression;
use api\models\Token;
use api\models\OrderDeal;
use api\models\OrderStrategy;
use api\models\Pair;
use api\models\Stock;
use api\models\StockPair;
use api\models\StockAccount;
use api\models\StockAccountBalance;
use api\models\StatCoinmarketcap;

// use api\models\PlaylistsItems;
// use api\models\PlaylistsItemsParts;

// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en
// http://demohost.com:8888/signup?email=maaffa@adffdil.cfom&password=123&language=en
// http://demohost.com:8888/login?email=maaffa@fdil.cfom&password=123
// http://demohost.com:8888/token/KFwvIADhmDY7C06y3_77vSi9BgQMcc7J





header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");




class AccountsController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }


    /**
     * Displays JSON videos.
     *
     * @return string
     */

     public function actionStock_accounts($token=false)
     {
         if($token) {
             $user_id = Token::getUser($token);
             if($user_id) {

                 $modelAccounts = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0])->with(['stock','balance'])->asArray()->all();
                 $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();

                 $arrAccounts = [];
                 foreach ($modelAccounts as $account) {


                     $someUpdatedDate = false;

                     // SUM BALANCE IN USD
                     $arrBalance = 0;
                     foreach ($account['balance'] as $balance) {
                         foreach ($modelCoinmarketcap as $coinmarketcap) {
                             if($balance['coin_label'] == $coinmarketcap['coin_label']) {
                                 $price_usd = $coinmarketcap['price_usd'];
                                 $available = $balance['available'];
                                 $arrBalance += $available * $price_usd;

                                 $someUpdatedDate = $balance['updated_at']; //lifehack
                             }
                         }
                     }


                     $arrAccounts[] = [
                         "id" => $account['id'],
                         "name" => $account['name'],
                         "email" => $account['email'],
                         "stock_name" => $account['stock']['name'],
                         "total_balance_usd" => round($arrBalance, 0),
                         "updated_at" => dateConverter($someUpdatedDate),
                         "created_at" => dateConverter($account['created_at']),
                     ];
                 }

                 $result = array (
                     "status" => 200,
                     "results" => $arrAccounts,
                 );
                 // print_r($result);
                 print_r( json_encode($result) );
                 exit();


             }
         }
     }

     public function actionStock_accounts_form($token=false, $id=false)
     {
         if($token) {
             $user_id = Token::getUser($token);
             if($user_id) {

                 $modelStocks = Stock::find()->asArray()->all();
                 $arrStocks = [];

                 $arrBalance = [];
                 $arrAccount = false;
                 if($id == 0) {
                     $arrAccount = [
                         "id" => 0,
                         "name" => "",
                         "email" => "",
                         "stock_id" => false,
                         "stock_api" => "",
                     ];

                     foreach ($modelStocks as $stock) {
                         $arrStocks[] = [
                             "stock_id" => $stock['id'],
                             "stock_name" => $stock['name'],
                             "stock_api" => $stock['api'],
                         ];
                     }

                 } else {
                     $modelAccount = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0, 'id' => $id])->with(['stock', 'balance'])->asArray()->one();
                     $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();


                     foreach ($modelAccount['balance'] as $balance) {

                         $price_usd = false;
                         foreach ($modelCoinmarketcap as $coinmarketcap) {
                             if($coinmarketcap['coin_id'] == $balance['coin_id']) {
                                 $price_usd = $coinmarketcap['price_usd'];
                             }
                         }
                         if($price_usd) {
                             $price_usd = $price_usd * $balance['available'];
                             $price_usd = round($price_usd, 2);
                         }

                         $arrBalance[] = [
                            "id" => $balance['id'],
                            "account_id" => $balance['account_id'],
                            "coin_id" => $balance['coin_id'],
                            "coin_label" => $balance['coin_label'],
                            "available" => $balance['available'],
                            "on_order" => $balance['on_order'],
                            "price_usd" => $price_usd,
                            "updated_at" => dateConverter($balance['updated_at']),
                         ];
                     }

                     $arrAccount = [
                         "id" => $modelAccount['id'],
                         "name" => $modelAccount['name'],
                         "email" => $modelAccount['email'],
                         "stock_id" => $modelAccount['stock']['id'],
                         "stock_api" => $modelAccount['stock_api'],
                     ];

                     foreach ($modelStocks as $stock) {
                         if($stock['id'] == $modelAccount['stock']['id']) {
                             $arrStocks[] = [
                                 "stock_id" => $stock['id'],
                                 "stock_name" => $stock['name'],
                                 "stock_api" => $stock['api'],
                             ];
                         }

                     }
                 }



                 usort($arrBalance,function($first,$second){
                     return $first['price_usd'] < $second['price_usd'];
                 });


                 $result = array (
                     "status" => 200,
                     "account" => $arrAccount,
                     "stocks" => $arrStocks,
                     "balance" => $arrBalance,
                 );
                 print_r( json_encode($result) );
                 exit();


             }
         }
     }

     public function actionStock_accounts_form_save($token=false)
     {
         if($token) {
             $user_id = Token::getUser($token);
             if($user_id) {

                 $array = file_get_contents("php://input");
                 $fp = fopen('array.json', 'w');
                 fwrite($fp, print_r($array, TRUE));
                 fclose($fp);

                 $json = preg_replace( "/\p{Cc}*$/u", "", $array);
                 $account = json_decode($json, TRUE);

                 if(count($account)>0) {

                     // Check strlen fields
                     if(strlen($account['account_name']) < 3 || strlen($account['account_email']) < 3) {
                         $result = array (
                             "status" => 300,
                             "message" => 'Short name or email',
                         );
                         print_r( json_encode($result) );
                         exit();
                     }


                     $account_id = $account['account_id'];

                     if($account_id == 0) { // NEW ACCOUNT


                         $modelAccount = new StockAccount();
                         $modelAccount->name = $account['account_name'];
                         $modelAccount->email = $account['account_email'];
                         $modelAccount->user_id = $user_id;
                         $modelAccount->stock_id = $account['account_stock_id'];
                         $modelAccount->stock_api = $account['account_stock_api'];
                         $modelAccount->deleted = 0;
                         $modelAccount->created_at = time();
                         $modelAccount->updated_at = time();
                         $modelAccount->save();
                         $accountId = $modelAccount->id;


                         // CREATE WALLETS BY COIN_FROM OF THE CURRENT STOCK
                         $stockId = $account['account_stock_id'];
                         $stockCoins = [];

                         $modelPair = StockPair::find()->where(['stock_id' => $stockId])->with(['pair_coin'])->asArray()->all();

                         foreach ($modelPair as $pair) {
                             $stockCoins[] = [
                                 "coin_label" => $pair['pair_coin']['coin_from']['iso'],
                                 "coin_id" => $pair['pair_coin']['coin_from']['id'],
                             ];
                             $stockCoins[] = [
                                 "coin_label" => $pair['pair_coin']['coin_to']['iso'],
                                 "coin_id" => $pair['pair_coin']['coin_to']['id'],
                             ];
                         }

                         $stockCoins = array_map("unserialize", array_unique(array_map("serialize", $stockCoins)));
                         // print_r($stockCoins);
                         // exit();

                         foreach ($stockCoins as $coin) {
                             if(
                                 $coin['coin_label'] != null &&
                                 $coin['coin_id'] != null
                             ) {
                                 $modelBalance = new StockAccountBalance();
                                 $modelBalance->coin_id = $coin['coin_id'];
                                 $modelBalance->account_id = $accountId;
                                 $modelBalance->coin_label = $coin['coin_label'];
                                 $modelBalance->available = 0;
                                 $modelBalance->on_order = 0;
                                 $modelBalance->updated_at = time();
                                 $modelBalance->save();
                             }

                         }

                         $result = array (
                             "status" => 200,
                             "message" => 'Account added!',
                         );
                         print_r( json_encode($result) );
                         exit();

                     } else { // UPDATE ACCOUNT

                         $modelAccount = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0, 'id' => $account_id])->with(['stock', 'balance'])->one();
                         if(count($modelAccount)>0) {
                             $modelAccount->name = $account['account_name'];
                             $modelAccount->email = $account['account_email'];
                             $modelAccount->stock_id = $account['account_stock_id'];
                             $modelAccount->stock_api = $account['account_stock_api'];
                             $modelAccount->updated_at = time();
                             $modelAccount->save();

                             if($account['balance_update']) {
                                 $coin_label = $account['balance_update']['coin_label'];
                                 $available = $account['balance_update']['available'];
                                 $on_order = $account['balance_update']['on_order'];

                                 $modelBalance = StockAccountBalance::find()->where(['coin_label' => $coin_label, 'account_id' => $account_id])->one();
                                 $modelBalance->available = $available;
                                 $modelBalance->on_order = $on_order;
                                 $modelBalance->save();
                             }


                             $result = array (
                                 "status" => 200,
                                 "message" => 'Account saved!',
                             );
                             print_r( json_encode($result) );
                             exit();
                         } else {
                             $result = array (
                                 "status" => 300,
                                 "message" => 'Account not found',
                             );
                             print_r( json_encode($result) );
                             exit();
                         }
                     }
                 } else {
                     $result = array (
                         "status" => 300,
                         "message" => 'Post request not found',
                     );
                     print_r( json_encode($result) );
                     exit();
                 }




             }
         }
     }


}



function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}

function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}

function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
