<?php

namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use api\models\Token;
use api\models\OrderDeal;
use api\models\OrderCase;
use api\models\OrderStrategy;
use api\models\Coin;
use api\models\Pair;
use api\models\Stock;
use api\models\StockOrder;
use api\models\StockAccount;
use api\models\StockPair;
use api\models\StockAccountBalance;
use api\models\StatCoinmarketcap;

use api\models\User;
use api\models\UserFilters;
use api\models\UserFiltersBlacklist;
use api\models\Signup;


use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\TelegramLog;
use Longman\TelegramBot\Request;


header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

class StockController extends Controller
{

    public $tmp_orders_sell = [];
    public $tmp_orders_buy = [];
    public $tmp_transactions = [];
    public $tmp_group_counter = 0;

    public function combTransactions() {


        // print_r($this->tmp_orders_sell);
        // exit();

        $balance = 0;
        $stop = false;

        foreach ($this->tmp_orders_buy as $key_b => $buy) {
            // print_r($buy);
            // exit();

            foreach ($this->tmp_orders_sell as $key_s => $sell) {

                if($stop) { return false; }

                $price_usd_sell = $sell['_META']['price_usd'];
                $price_usd_buy = $buy['_META']['price_usd'];
                $total_usd_sell = $sell['_META']['total_usd'];
                $total_usd_buy = $buy['_META']['total_usd'];

                $buy_balance = $total_usd_buy - $total_usd_sell;

                $profit_usd_sell = $total_usd_sell-(($price_usd_sell/$price_usd_buy)*$total_usd_sell);
                $profit_usd_buy = $total_usd_buy-(($price_usd_sell/$price_usd_buy)*$total_usd_buy);


                // $profit_percent = ((($price_usd_buy - $price_usd_sell) / ($price_usd_buy + $price_usd_sell)) / 2) * 100;
                $profit_percent_sell = ($profit_usd_sell * 100)/$total_usd_sell;
                $profit_percent_buy = ($profit_usd_buy * 100)/$total_usd_buy;


                if($buy_balance >= 0) {
                    $this->tmp_orders_buy[$key_b]['_META']['total_usd'] = $buy_balance;
                    unset($this->tmp_orders_sell[$key_s]);
                    if($sell['_META']['total_usd'] > 0 && $profit_percent_sell > 0) {
                        $this->tmp_transactions[$this->tmp_group_counter][] = [
                            "volume" => $total_usd_sell,
                            "profit_percent" => $profit_percent_sell,
                            "profit_usd" => $profit_usd_sell,
                        ];
                    }

                    $stop = true;
                    $this->combTransactions();
                } else {
                    $this->tmp_orders_sell[$key_s]['_META']['total_usd'] =- $buy_balance;
                    unset($this->tmp_orders_buy[$key_b]);
                    if($buy['_META']['total_usd'] > 0 && $profit_percent_buy > 0) {
                        $this->tmp_transactions[$this->tmp_group_counter][] = [
                            "volume" => $total_usd_buy,
                            "profit_percent" => $profit_percent_buy,
                            "profit_usd" => $profit_usd_buy,
                        ];
                        $this->tmp_group_counter++;
                    }

                    $stop = true;
                    $this->combTransactions();
                }
            }
        }

        // print_r($this->tmp_transactions);
        // exit();

        return $this->tmp_transactions;
    }

    public function ordersPresets($action=false)
    {

        $modelWhitelist = StockPair::find()->where(['whitelist' => 1])->asArray()->all();
        $whitelist = [];
        foreach ($modelWhitelist as $pair) {
            $whitelist[] = $pair['id'];
        }


        $date = \Yii::$app->params['lastOrdersDate'];
        $modelOrders = StockOrder::find()->where(['>', 'created_at', $date])->with(['stock','pair'])->asArray()->all();


                                // EXPERIMENT: GET SOME ORDERS
                                /*
                                $limitArray = [];
                                foreach ($modelOrders as $order) {
                                    $pair = $order['pair']['coin_from']."_".$order['pair']['coin_to'];
                                    $stock = $order['stock']['name'];
                                    $limitArray[$pair]['SELL'][$stock] = []; //Sell
                                    $limitArray[$pair]['BUY'][$stock] = []; //Buy
                                }
                                foreach ($modelOrders as $order) {
                                    $pair = $order['pair']['coin_from']."_".$order['pair']['coin_to'];
                                    $pair_id = $order['pair_id'];
                                    if(in_array($pair_id, $whitelist)) {
                                        $stock = $order['stock']['name'];
                                        $type = $order['type'] ? 'BUY' : 'SELL';
                                        if(count($limitArray[$pair][$type][$stock]) < 3) {
                                            $limitArray[$pair][$type][$stock][] = $order;
                                        }
                                    }
                                }
                                // print_r($limitArray);
                                // exit();

                                $mergeArray = [];
                                foreach ($limitArray as $pair) {
                                    foreach ($pair as $type) {
                                        foreach ($type as $stock) {
                                            $mergeArray[] = $order;
                                        }
                                    }
                                }

                                $modelOrders = $mergeArray;
                                */



        // IF PAIR IN WHITELIST
        $ordersWhitlist = [];
        foreach ($modelOrders as $order) {
            $pair_id = $order['pair_id'];
            if(in_array($pair_id, $whitelist)) {
                $ordersWhitlist[] = $order;
            }
        } $modelOrders = []; //clean memory



        // _META: RECALC TO USD
        $ordersMeta = [];
        $prices = getCurrencies();

        foreach ($ordersWhitlist as $key => $order) {
            $coin_to = $order['pair']['coin_to'];
            $priceFounded = false;

            foreach ($prices as $price) {
                if($coin_to == $price['coin']) {
                    $ordersMeta[] = $order;
                    $orderIndex = count($ordersMeta)-1;
                    $priceFounded = false;

                    $priceInUsd = $order['price'] * $price['price_usd'];
                    $priceInBtc = $order['price'] * $price['price_btc'];

                    $ordersMeta[$orderIndex]['_META'] = [
                        "price_usd" => $priceInUsd,
                        "total_usd" => $priceInUsd * $order['volume'],
                        "price_btc" => $priceInBtc,
                        "total_btc" => $priceInBtc * $order['volume'],
                    ];
                    $priceFounded = true;
                }
            }
            // if(!$priceFounded) {
            //     $ordersMeta[$orderIndex]['_META'] = [
            //         "price_usd" => 0,
            //         "total_usd" => 0,
            //     ];
            // }
        }



        // SORT ORDERS BY COIN_FROM
        $ordersCoinFrom = [];

        foreach ($ordersMeta as $key => $order) {
            $stock = $order['stock']['name'];
            $coin_from = $order['pair']['coin_from'];
            $ordersCoinFrom[$coin_from][$stock] = [
                "SELL" => [],
                "BUY" => [],
            ];
        }
        foreach ($ordersMeta as $key => $order) {
            $stock = $order['stock']['name'];
            $type = $order['type'];
            $type = $type == 1 ? 'BUY' : 'SELL';
            $coin_from = $order['pair']['coin_from'];

            $ordersCoinFrom[$coin_from][$stock][$type][] = $order;
        }
        $ordersMeta = []; //clean memory




        // SORT COMBINATIONS
        $ordersCombSort = [];
        foreach ($ordersCoinFrom as $key => $stock) {
            foreach ($stock as $key_stock => $orders) {

                $orders_sell = $orders['SELL'];
                $orders_buy = $orders['BUY'];

                $ordersCombSort[$key][$key_stock]['SELL'] = [];
                $ordersCombSort[$key][$key_stock]['BUY'] = [];

                if(count($orders_sell) > 0) {
                    usort($orders_sell,function($first,$second){
                        return $first['price'] > $second['price'];
                    });
                    $ordersCombSort[$key][$key_stock]['SELL'] = $orders_sell;
                }
                if(count($orders_buy) > 0) {
                    usort($orders_buy,function($first,$second){
                        return $first['price'] < $second['price'];
                    });
                    $ordersCombSort[$key][$key_stock]['BUY'] = $orders_buy;
                }

            }

        } $ordersCoinFrom = []; //clean memory




        // MAKE ORDERS COMBINATION BY STOCK
        $ordersComb = [];
        foreach ($ordersCombSort as $key_coin_from => $stock) {

            foreach ($stock as $key_sell => $type_sell) {

                foreach ($stock as $key_buy => $type_buy) {
                    if(
                        $key_sell != $key_buy &&
                        count($type_sell['SELL']) > 0 &&
                        count($type_buy['BUY']) > 0
                    ) {
                        $label_sell = $key_sell." ".$key_coin_from."_".$type_sell['SELL'][0]['pair']['coin_to'];
                        $label_buy = $key_buy." ".$key_coin_from."_".$type_buy['BUY'][0]['pair']['coin_to'];


                        // GET ORDERS WHEN FIRST ORDER_BUY > FIRST ORDER_SELL
                        $ordersBuyArr = [];
                        foreach ($type_buy['BUY'] as $order_buy) {

                            $price_buy = $order_buy['_META']['price_usd'];
                            $price_sell = $type_sell['SELL'][0]['_META']['price_usd'];

                            if($price_buy > $price_sell) {
                                $ordersBuyArr[] = $order_buy;
                            }
                        }

                        if(count($ordersBuyArr) > 0) {
                            $ordersCombKey = $label_sell." >> ".$label_buy;
                            $ordersComb[$ordersCombKey] = [
                                'SELL' => array_slice($type_sell['SELL'], 0, 10),
                                'BUY' => array_slice($ordersBuyArr, 0, 10),
                            ];
                        }

                    }
                }

            }

        }

        $ordersCombSort = []; //clean memory



        // PROFIT CALCULATION
        $ordersCombWithProfit = [];

        foreach ($ordersComb as $key => $comb) {
            // print_r($comb);
            $this->tmp_group_counter = 0;
            $this->tmp_transactions = [];
            $this->tmp_orders_sell = $comb['SELL'];
            $this->tmp_orders_buy = $comb['BUY'];
            $this->combTransactions();

            $ordersCombWithProfit[] = [
                "SELL" => $comb['SELL'],
                "BUY" => $comb['BUY'],
                "PROFIT" => [
                    "stat" => [],
                    "groups" => $this->tmp_transactions,
                ],
                "sound" => false,
            ];
        } $ordersCombSort = []; //clean memory


        // CALCULATE PROFIT
        $ordersCombProfitCalculate = [];
        foreach ($ordersCombWithProfit as $key => $comb) {
            $total_volume = 0;
            $total_profit_usd = 0;
            $total_percent = [];

            foreach ($comb['PROFIT']['groups'] as $block) {
                foreach ($block as $profit) {
                    $total_volume = $total_volume + $profit['volume'];
                    $total_profit_usd = $total_profit_usd + $profit['profit_usd'];
                    $total_percent[] = $profit['profit_percent'];
                }
            }


           if($total_profit_usd > 10) {
               $ordersCombProfitCalculate[] = $comb;
               $combIndex = count($ordersCombProfitCalculate)-1;

               $ordersCombProfitCalculate[$combIndex]['PROFIT']['stat'] = [
                   "total_volume" => $total_volume,
                   "total_profit_usd" => $total_profit_usd,
                   "median" => median($total_percent),
               ];
           }

        } $ordersCombWithProfit = []; //clean memory

        // exit();
        return $ordersCombProfitCalculate;

    }

    public function JsonBeautifierOrders($orders)
    {

        // print_r($orders);
        // exit();
        $block = [
            "data" => [],
            "stat" => [],
            "orders" => [],
        ];

        foreach ($orders as $key => $order) {
            if($key == 0) {
                $block['data'] = [
                    "stock_id" => (int)$order['stock']['id'],
                    "stock_name" => $order['stock']['name'],
                    "stock_fast_link" => fastLinkModifier($order['stock']['fast_link'], $order['pair']['coin_from'], $order['pair']['coin_to']),
                    "pair_id" => false,
                    "stock_pair_id" => (int)$order['pair_id'],
                    "pair" => [
                        "coin_from" => $order['pair']['coin_from'],
                        "coin_to" => $order['pair']['coin_to'],
                    ],
                ];
            }
        }

        $ordersArr = [];
        foreach ($orders as $key => $order) {
            // print_r($order);
            // exit();
            $volume = $order['volume'];
            $price = $order['price'];
            $price_usd = $order['_META']['price_usd'];
            $price_btc = $order['_META']['price_btc'];

            $ordersArr[] = [
                "type" => (int)$order['type'],
                "order_id" => (int)$order['id'],
                "timestamp" => dateConverter($order['created_at']),
                "volume" => $volume,
                "price" => $price,
                "total" => $volume * $price,
                "coin_from" => $order['pair']['coin_from'],
                "coin_to" => $order['pair']['coin_to'],
                "usd" => [
                    "price" => $price_usd,
                    "total" => $price_usd * $volume,
                ],
                "btc" => [
                    "price" => $price_btc,
                    "total" => $price_btc * $volume,
                ],
            ];
        }

        $block['orders'] = $ordersArr;

        $stat_volume = 0;
        $stat_total = 0;
        foreach ($orders as $key => $order) {
            $volume = $order['volume'];
            $price_usd = $order['_META']['price_usd'];

            $stat_volume += $volume;
            $stat_total += $volume * $price_usd;
        }

        $block['stat'] = [
            "volume" => $stat_volume,
            "total" => $stat_total,
        ];

        return $block;

    }



    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */



    public function actionEqualizer_json()
    {

        $ordersCombs = $this->ordersPresets();

        // CONVERT TO JSON
        $ordersCombJson = [];
        foreach ($ordersCombs as $key => $comb) {

            $ordersCombJson[] = [
                "sound" => false,
                "sell" => $this->JsonBeautifierOrders($comb['SELL']),
                "buy" => $this->JsonBeautifierOrders($comb['BUY']),
                "profit" => $comb['PROFIT'],
            ];
        }

        $ordersCombWithProfit = $ordersCombJson;


        // SET STATISTICS FROM COINMARKETCAP
        $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
        foreach ($ordersCombWithProfit as $key => $comb) {
            $stat_coin = $comb['sell']['data']['pair']['coin_from'];
            $ordersCombWithProfit[$key]['coinmarketcap'] = [
                "coin_label" => $stat_coin,
                "link" => 0,
                "rank" => 0,
                "percent_change_1h" => 0,
                "percent_change_24h" => 0,
                "percent_change_7d" => 0,
                "updated_at" => dateConverter(time()),
            ];

            foreach ($modelCoinmarketcap as $coin) {
                if($coin['coin_label'] == $stat_coin) {
                    $ordersCombWithProfit[$key]['coinmarketcap'] = [
                        "coin_label" => $coin['coin_label'],
                        "link" => "https://coinmarketcap.com/currencies/".$coin['name']."/",
                        "rank" => $coin['rank'],
                        "percent_change_1h" => $coin['percent_change_1h'],
                        "percent_change_24h" => $coin['percent_change_24h'],
                        "percent_change_7d" => $coin['percent_change_7d'],
                        "updated_at" => dateConverter($coin['updated_at']),
                    ];
                }
            }
        }

        $array = json_encode($ordersCombWithProfit);
        $json = file_get_contents("php://input");
        $fp = fopen('json/equalizer.json', 'w');
        fwrite($fp, print_r($array, TRUE));
        fclose($fp);


        print_r($array);
        exit();
    }

    public function actionEqualizer_return()
    {
        return json_decode(file_get_contents("json/equalizer.json"), true);
    }


    public function actionIndex($action=false)
    {
        // if($token) {
        //     $user_id = Token::getUser($token);
        //     if($user_id) {
        //     }
        // }

        $ordersCombWithProfit = $this->actionEqualizer_return();
        $result = [];
        if(is_array($ordersCombJson) || is_object($ordersCombJson)) {
            $result = array (
                "status" => 200,
                "results" => $ordersCombJson
            );
        } else {
            $result = array (
                "status" => 200,
                "message" => 'Array is broken',
            );
        }
        print_r( json_encode($result) );
    }

    public function equalizer_with_filters($user_id, $ordersCombWithProfit)
    {

        $modelUserFilters = UserFilters::find()->where(['user_id' => $user_id, 'deleted' => 0])->asArray()->all();



        $filteringCombs = [];
        if(count($modelUserFilters)>0) {

            $filterRules = [];
            foreach ($modelUserFilters as $filter) {

                $userStocks = strlen($filter['stocks']) > 0 ? explode(',',$filter['stocks']) : [];
                $userCoins = strlen($filter['coins']) > 0 ? explode(',',$filter['coins']) : [];

                $filterRules[] = [
                    "min_price" => $filter['min_price'],
                    "min_percent" => $filter['min_percent'],
                    "sound" => $filter['sound'],
                    "same" => $filter['same'],
                    "stocks" => $userStocks,
                    "coins_match" => $userCoins,
                ];
            }

            foreach ($ordersCombWithProfit as $key => $comb) {

                $combFounded = false;
                $hasSound = false;
                $hasSameCoinsTo = false;
                $stocksFounded = 0;
                $coinsFounded = false;

                foreach ($filterRules as $rule) {

                    // Filterings by min price/percent
                    if(
                        $comb['profit']['stat']['total_profit_usd'] >= $rule['min_price'] &&
                        $comb['profit']['stat']['median'] >= $rule['min_percent']
                    ) {
                        $combFounded = true;
                        if($rule['sound']) {
                            $hasSound = true;
                        }
                    }

                    // Filterings by available stocks
                    foreach ($rule['stocks'] as $stock) {
                        if(
                            $comb['sell']['data']['stock_id'] == $stock ||
                            $comb['buy']['data']['stock_id'] == $stock
                        ) {
                            $stocksFounded++;
                        }
                    }
                    if(count($rule['stocks']) == 0) {
                        $stocksFounded = true;
                    }

                    // Filterings by available coins_to
                    foreach ($rule['coins_match'] as $coin) {
                        if(
                            $comb['sell']['data']['pair']['coin_to'] == $coin &&
                            $comb['buy']['data']['pair']['coin_to'] == $coin
                        ) {
                            $coinsFounded = true;
                        }
                    }
                    if(count($rule['coins_match']) == 0) {
                        $coinsFounded = true;
                    }

                    // Filtering by same coins_to
                    if($rule['same']) {
                        if($comb['sell']['data']['pair']['coin_to'] == $comb['buy']['data']['pair']['coin_to']) {
                            $hasSameCoinsTo = true;
                        } else {
                            $hasSameCoinsTo = false;
                        }
                    } else {
                        $hasSameCoinsTo = true;
                    }

                }
                // print_r("combFounded: ".$combFounded." stocksFounded: ".$stocksFounded." coinsFounded: ".$coinsFounded);
                if($combFounded && $stocksFounded == 2 && $coinsFounded && $hasSameCoinsTo) {
                    $filteringCombs[] = $comb;
                    $combIndex = count($filteringCombs)-1;
                    $filteringCombs[$combIndex]['sound'] = $hasSound;
                }
            }

        } else {
            $filteringCombs = $ordersCombWithProfit;
        }

        // REMOVE COMBS IF COIN_FROM IN BLACKLIST
        $modelFiltersBlacklist = UserFiltersBlacklist::find()->where(['user_id' => $user_id])->asArray()->one();
        if(count($modelFiltersBlacklist) > 0) {
            $ordersWithinCoins = [];
            $userCoins = explode(',',$modelFiltersBlacklist['array']);
            foreach ($filteringCombs as $comb) {
                $coin_from = $comb['sell']['data']['pair']['coin_from'];
                // $blacklist = $modelFiltersBlacklist['array']
                $foundInBlacklist = false;
                foreach ($userCoins as $coin) {
                    if($coin_from == $coin) {
                        $foundInBlacklist = true;
                    }
                }
                if(!$foundInBlacklist) {
                    $ordersWithinCoins[] = $comb;
                }
            }
            $filteringCombs = $ordersWithinCoins;
        }



        // Sorting by params
        usort($filteringCombs,function($first,$second){ //Default sort by median (percent)
            return $first['profit']['stat']['median'] < $second['profit']['stat']['median'];
        });


        return $filteringCombs;
    }



    public function getCalculationOrdersByDate()
    {
        $lastOrdersDate = \Yii::$app->params['lastOrdersDate'];
        $sysData = [
            "updated_at" => dateConverter($lastOrdersDate),
            "timer" => \Yii::$app->params['lastOrdersDateStr'],
            "orders" => [],
        ];

        $modelOrders = StockOrder::find()->where(['>', 'created_at', $lastOrdersDate])->with(['stock','pair'])->asArray()->all();
        $ordersSysCalc = [];
        foreach ($modelOrders as $order) {
            $stock_name = $order['stock']['name'];
            $ordersSysCalc[$stock_name] = [
                "stock_name" => $stock_name,
                "orders" => [],
            ];
        }
        foreach ($modelOrders as $order) {
            $stock_name = $order['stock']['name'];
            $coin_to = $order['pair']['coin_to'];
            $coin_from = $order['pair']['coin_from'];
            $ordersSysCalc[$stock_name]['orders'][$coin_to][$coin_from][] = $order;
        }

        $ordersSysCalcInt = [];
        foreach ($ordersSysCalc as $stock) {
            $stock_name = $stock['stock_name'];
            $ordersSysCalcInt[$stock_name] = [
                "stock_name" => $stock_name,
                "orders_counter" => 0,
                "groups" => [],
            ];

            foreach ($stock['orders'] as $key_pair_to => $pair) {
                $ordersSysCalcInt[$stock_name]['groups'][$key_pair_to] = [
                    "coin_to" => $key_pair_to,
                    "orders_counter" => 0,
                    "coin_from" => [],
                ];
                foreach ($pair as $key_pair_from => $order) {
                    $ordersSysCalcInt[$stock_name]['groups'][$key_pair_to]['coin_from'][$key_pair_from] = [
                        "pair_label" => $key_pair_from."_".$key_pair_to,
                        "orders_counter" => 0,
                    ];
                }
                foreach ($pair as $key_pair_from => $orders) {
                    foreach ($orders as $order) {
                        $label = $key_pair_from."_".$key_pair_to;
                        $ordersSysCalcInt[$stock_name]['groups'][$key_pair_to]['coin_from'][$key_pair_from]['orders_counter']++;
                        $ordersSysCalcInt[$stock_name]['groups'][$key_pair_to]['orders_counter']++;
                        $ordersSysCalcInt[$stock_name]['orders_counter']++;
                    }
                }
            }
        }

        $sysData['orders'] = $ordersSysCalcInt;
        return $sysData;
    }


    public function actionOrders_for_deal($token)
    {

        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $stocks_id = isset($_GET['stocks_id']) ? explode(',', $_GET['stocks_id']) : false;
                $pair = isset($_GET['pair']) ? explode('_', $_GET['pair']) : false;
                $type = isset($_GET['type']) ? (int)$_GET['type'] : false;
                // if($type == 'sell') {$type = 0;} elseif($type == 'buy') {$type = 1;} else { $type = false; }


                // $modelStock = Stock::find()->where(['id' => $stocks_id])->asArray()->all();
                $modelPair = Pair::find()->where(['coin_from' => $pair[0], 'coin_to' => $pair[1]])->asArray()->one();
                $ordersArr = [
                    "pair" => $pair[0]."_".$pair[1],
                    "type" => $type,
                    "orders" => [],
                ];

                if(count($modelPair) > 0) {
                    $lastOrdersDate = \Yii::$app->params['lastOrdersDate'];
                    $modelOrders = StockOrder::find()->where(['>', 'created_at', $lastOrdersDate])->andWhere(['stock_id' => $stocks_id, 'pair_id' => $modelPair['id'], 'type' => $type])->with(['stock'])->asArray()->all();


                    $ordersByStock = [];
                    if(count($modelOrders) > 0) {

                        foreach ($modelOrders as $order) {
                            $stock_id = $order['stock_id'];

                            $ordersByStock[$stock_id][] = [
                                "stock_id" => $stock_id,
                                "stock_name" => $order['stock']['name'],
                                "fastlink" => fastLinkModifier($order['stock']['fast_link'], $pair[0], $pair[1]),
                                "price" => $order['price'],
                                "created_at" => dateConverter($order['created_at']),
                            ];
                        }
                    }



                    foreach ($ordersByStock as $key => $stock) {
                        if($type == 0) { //Sell
                            usort($ordersByStock[$key],function($first,$second){
                                return $first['price'] > $second['price'];
                            });
                        } elseif($type == 1) { //Buy
                            usort($ordersByStock[$key],function($first,$second){
                                return $first['price'] < $second['price'];
                            });
                        }
                    }

                    $extractedOrders = [];
                    foreach ($ordersByStock as $stock) {
                        if(count($stock) > 0) {
                            $extractedOrders[] = $stock[0];
                        }
                    }

                    if($type == 0) { //Sell
                        usort($extractedOrders,function($first,$second){
                            return $first['price'] > $second['price'];
                        });
                    } elseif($type == 1) { //Buy
                        usort($extractedOrders,function($first,$second){
                            return $first['price'] < $second['price'];
                        });
                    }

                    foreach ($stocks_id as $stock_id) {
                        $stock_founded = false;
                        foreach ($extractedOrders as $order) {
                            if($order['stock_id'] == $stock_id) {
                                $stock_founded = true;
                            }
                        }

                        if(!$stock_founded) {
                            $modelStock = Stock::find()->where(['id' => $stock_id])->asArray()->one();

                            $extractedOrders[] = [
                                "stock_id" => $stock_id,
                                "stock_name" => $modelStock['name'],
                                "fastlink" => fastLinkModifier($modelStock['fast_link'], $pair[0], $pair[1]),
                                "price" => false,
                                "created_at" => false,
                            ];
                        }
                    }


                    $ordersArr['orders'] = $extractedOrders;
                }


                $result = array (
                    "status" => 200,
                    "results" => $ordersArr,
                );
                print_r( json_encode($result) );
                exit();
            }
        }
    }


    public function actionOrders_by_stock($token)
    {

        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $stock_id = isset($_GET['stock_id']) ? $_GET['stock_id'] : false;
                $pair = isset($_GET['pair']) ? $_GET['pair'] : false; $pair = explode('_', $pair);
                $type = isset($_GET['type']) ? (int)$_GET['type'] : false;
                // if($type == 'sell') {$type = 0;} elseif($type == 'buy') {$type = 1;} else { $type = false; }


                $modelStock = Stock::find()->where(['id' => $stock_id])->asArray()->one();
                $stockData = [
                    "stock_name" => $modelStock['name'],
                    "fastlink" => fastLinkModifier($modelStock['fast_link'], $pair[0], $pair[1]),
                    "pair" => $pair[0]."_".$pair[1],
                ];



                $modelPairs = Pair::find()->where(['coin_from' => $pair[0], 'coin_to' => $pair[1]])->asArray()->one();
                $pair_id = $modelPairs['id'];

                $lastOrdersDate = \Yii::$app->params['lastOrdersDate'];
                $modelOrders = StockOrder::find()->where(['>', 'created_at', $lastOrdersDate])->andWhere(['stock_id' => $stock_id, 'pair_id' => $pair_id, 'type' => $type])->with('pair')->asArray()->all();

                $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
                $coinmarketcapArr = [];
                $ordersArr = [];
                foreach ($modelOrders as $order) {
                    $coin_from = $order['pair']['coin_from'];
                    $coin_to = $order['pair']['coin_to'];

                    $total = $order['volume'] * $order['price'];

                    $ordersArr[] = [
                        "price" => $order['price'],
                        "volume" => $order['volume'],
                        "total" => $total,
                        "created_at" => dateConverter($order['created_at']),
                        "usd" => [],

                    ];
                    $order_id = count($ordersArr) - 1;

                    // print_r($total);
                    // exit();
                    foreach ($modelCoinmarketcap as $coinmarketcap) {
                        if($coin_to == $coinmarketcap['coin_label']) {
                            $ordersArr[$order_id]['usd'] = [
                                "price" => $coinmarketcap['price_usd'],
                                "total" => $total * $coinmarketcap['price_usd'],
                            ];
                        }


                        if($coin_from == $coinmarketcap['coin_label']) {
                            $coinmarketcapArr['from'] = [
                                "link" => "https://coinmarketcap.com/currencies/".$coinmarketcap['name']."/",
                                "coin_label" => $coinmarketcap['coin_label'],
                                "percent_change_1h" => $coinmarketcap['percent_change_1h'],
                                "percent_change_24h" => $coinmarketcap['percent_change_24h'],
                                "percent_change_7d" => $coinmarketcap['percent_change_7d'],
                                "updated_at" => dateConverter($coinmarketcap['updated_at']),
                            ];
                        } elseif($coin_to == $coinmarketcap['coin_label']) {
                            $coinmarketcapArr['to'] = [
                                "link" => "https://coinmarketcap.com/currencies/".$coinmarketcap['name']."/",
                                "coin_label" => $coinmarketcap['coin_label'],
                                "percent_change_1h" => $coinmarketcap['percent_change_1h'],
                                "percent_change_24h" => $coinmarketcap['percent_change_24h'],
                                "percent_change_7d" => $coinmarketcap['percent_change_7d'],
                                "updated_at" => dateConverter($coinmarketcap['updated_at']),
                            ];
                        }
                    }
                    // print_r($order);
                    // exit();
                }
                if($type == 0) { //Sell
                    usort($ordersArr,function($first,$second){
                        return $first['price'] > $second['price'];
                    });
                } elseif($type == 1) { //Buy
                    usort($ordersArr,function($first,$second){
                        return $first['price'] < $second['price'];
                    });
                }

                // print_r($ordersArr);
                // exit();

                $result = array (
                    "status" => 200,
                    "stock" => $stockData,
                    "coinmarketcap" => $coinmarketcapArr,
                    "results" => $ordersArr,
                );
                print_r( json_encode($result) );
                exit();
            }
        }

        exit();
    }

    public function actionDelete_old_orders()
    {

        $dateToDelete = '-1 hour';
        $dateToDelete = strtotime($dateToDelete);
        // $countOrders = StockOrder::find()->where('created_at < :date', [':date' => $dateToDelete])->count();
        // $modelOrders = StockOrder::deleteAll('created_at < :date', [':date' => $dateToDelete]);

        $countOrders = StockOrder::find()->count();
        $modelOrders = StockOrder::deleteAll();

        print_r(
            ['Success!', $countOrders]
        );

        exit();
    }


    public function actionEqualizer($token)
    {

        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $params = [
                    "sort" => "percent",
                ];
                if(isset($_GET['sort'])) {
                    $params['sort'] = $_GET['sort'];
                }

                $ordersCombWithProfit = $this->actionEqualizer_return();

                // Sorting by fix
                if($params['sort'] == 'fix') {
                    usort($ordersCombWithProfit,function($first,$second){
                        return $first['profit']['stat']['total_profit_usd'] < $second['profit']['stat']['total_profit_usd'];
                    });
                }

                $ordersCombs = $this->equalizer_with_filters($user_id, $ordersCombWithProfit);

                $result = array (
                    "status" => 200,
                    "sys_data" => [
                        "combs_counter" => count($ordersCombWithProfit),
                        "global" => $this->getCalculationOrdersByDate(),
                    ],
                    "results" => $ordersCombs,
                );
                print_r( json_encode($result) );
                exit();

            }
        }
    }

    public function actionFilters($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $modelFilters = UserFilters::find()->where(['user_id' => $user_id, 'deleted' => 0])->asArray()->all();
                $modelStocks = Stock::find()->asArray()->all();


                $arrFilters = [];
                foreach ($modelFilters as $key => $filter) {
                    $sound = (int)$filter['sound'];
                    $sound = $sound == 1 ? true : false;

                    $same = (int)$filter['same'];
                    $same = $same == 1 ? true : false;

                    $arrFilters[] = [
                        "filter_id" => (int)$filter['id'],
                        "min_price" => (int)$filter['min_price'],
                        "min_percent" => (int)$filter['min_percent'],
                        "sound" => $sound,
                        "same" => $same,
                        "deleted" => 0,
                        "menu" => null,
                        "stocks" => [],
                        "coins_match" => [],
                    ];
                    $arrFiltersId = count($arrFilters)-1;

                    // print_r($filter['id']);

                    $userCoins = explode(',',$filter['coins']);
                    $globalCoins = \Yii::$app->params['globalCoins'];
                    foreach ($globalCoins as $coin) {

                        $coinActive = false;
                        foreach ($userCoins as $userCoin) {
                            if($coin == $userCoin) {
                                $coinActive = true;
                            }
                        }

                        $arrFilters[$arrFiltersId]['coins_match'][] = [
                            "label" => $coin,
                            "active" => $coinActive,
                        ];
                    }

                    $stocks = explode(',',$filter['stocks']);
                    $userStocks = [];
                    // print_r($stocks);
                    // exit();
                    foreach ($modelStocks as $stock) {

                        $status = false;
                        foreach ($stocks as $s) {
                            if($s == $stock['id']) {
                                $status = true;
                                // print_r('TRUE!');
                            }
                        }
                        // $status = array_search($stock['id'], $stocks);
                        // $status = $status >= 0 ? true : false;
                        // print_r($status);

                        $userStocks[] = [
                            "stock_id" => $stock['id'],
                            "stock_name" => $stock['name'],
                            "active" => $status,
                        ];
                    }
                    // usort($userStocks,function($first,$second){
                    //     return $first['stock_name'] > $second['stock_name'];
                    // });
                    usort($userStocks,function($first,$second){
                        return $first['active'] < $second['active'];
                    });

                    $arrFilters[$arrFiltersId]['stocks'] = $userStocks;
                    // exit();
                    // print_r(
                    //     $userStocks
                    // );
                    // exit();
                    // min_price: 0,
                    // min_percent: 0,
                    // sound: false,
                    // menu: null,
                }

                // print_r($arrFilters);
                // exit();



                // GENERATE COINS BLACKLIST
                $modelCoins = Coin::find()->asArray()->all();
                $modelFiltersBlacklist = UserFiltersBlacklist::find()->where(['user_id' => $user_id])->asArray()->one();
                $user_coins = explode(',', $modelFiltersBlacklist['array']);
                $arrCoins = [];
                foreach ($modelCoins as $coin) {
                    $coin_active = false;
                    foreach ($user_coins as $user_coin) {
                        if($coin['iso'] == $user_coin) {
                            $coin_active = true;
                        }
                    }
                    if(strlen($coin['iso']) > 0) {
                        $arrCoins[] = [
                            "active" => $coin_active,
                            "label" => $coin['iso'],
                        ];
                    }

                }
                usort($arrCoins,function($first,$second){
                    return $first['label'] > $second['label'];
                });
                usort($arrCoins,function($first,$second){
                    return $first['active'] < $second['active'];
                });

                $result = array (
                    "status" => 200,
                    // "prices" => $prices,
                    "blacklist" => $arrCoins,
                    "filters" => $arrFilters,
                );
                print_r( json_encode($result) );
                exit();
            }
        }
    }


    public function actionBlacklist_save($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {
                $json = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($json, TRUE));
                fclose($fp);

                // $json = preg_replace( "/\p{Cc}*$/u", "", $json);
                $blacklist = json_decode($json, TRUE);

                if($blacklist != null) {
                    $blacklist = join(',',$blacklist);
                } else {
                    $blacklist = "";
                }

                $modelFiltersBlacklist = UserFiltersBlacklist::find()->where(['user_id' => $user_id])->one();
                if(count($modelFiltersBlacklist) > 0) {
                    $modelFiltersBlacklist->array = $blacklist;
                    $modelFiltersBlacklist->save();
                } else {
                    $blacklistModel = new UserFiltersBlacklist();
                    $blacklistModel->user_id = $user_id;
                    $blacklistModel->array = $blacklist;
                    $blacklistModel->save();
                }

                $result = array (
                    "status" => 200,
                    "message" => 'Blacklist saved!',
                );
                print_r( json_encode($result) );
                exit();
            }
        }
        exit();
    }

    public function actionFilters_save($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $json = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($json, TRUE));
                fclose($fp);


                $json = preg_replace( "/\p{Cc}*$/u", "", $json);
                $filter = json_decode($json, TRUE);


                if(count($filter)>0) {

                    $sound = $filter['sound'] == true ? 1 : 0;
                    $same = $filter['same'] == true ? 1 : 0;

                    $model = UserFilters::find()->where(['user_id' => $user_id, 'id' => $filter['filter_id']])->one();
                    if(count($model)>0) {

                        $stocks = [];
                        foreach ($filter['stocks'] as $stock) {
                            if($stock['active']) {
                                $stocks[] = $stock['stock_id'];
                            }
                        }
                        $stocks = join(',',$stocks);

                        $coins = [];
                        foreach ($filter['coins_match'] as $coin) {
                            if($coin['active']) {
                                $coins[] = $coin['label'];
                            }
                        }
                        $coins = join(',',$coins);

                        $model->min_price = $filter['min_price'];
                        $model->min_percent = $filter['min_percent'];
                        $model->deleted = $filter['deleted'];
                        $model->sound = $sound;
                        $model->same = $same;
                        $model->stocks = $stocks;
                        $model->coins = $coins;
                        $model->updated_at = time();
                        $model->save();
                    } else {
                        $model = new UserFilters();
                        $model->user_id = $user_id;
                        $model->min_price = $filter['min_price'];
                        $model->min_percent = $filter['min_percent'];
                        $model->deleted = 0;
                        $model->sound = $sound;
                        $model->same = $same;
                        $model->created_at = time();
                        $model->updated_at = time();
                        $model->save();
                    }

                    $result = array (
                        "status" => 200,
                        "message" => 'Filters saved!',
                    );
                    print_r( json_encode($result) );
                    exit();
                } else {
                    $result = array (
                        "status" => 200,
                        "message" => 'Post request not found!',
                    );
                    print_r( json_encode($result) );
                    exit();
                }


                $result = array (
                    "status" => 200,
                    "message" => 'Filters saved!',
                );
                print_r( json_encode($result) );
                exit();


            }
        }
    }

    public function actionTelegram()
    {


       // $db = Yii::$app->getDb_telegram();
       $db = \Yii::$app->params['telegram']['db'];
       $bot_api_key  = \Yii::$app->params['telegram']['bot_api_key'];
       $bot_username = \Yii::$app->params['telegram']['bot_username'];

       $hook_url = getHomeUrl().'/telegram_hook';
       // print_r($hook_url);

       try {
           // $result = $telegram->deleteWebhook();

           // Create Telegram API object
           $telegram = new Telegram($bot_api_key, $bot_username);

           // Enable MySQL
           $telegram->enableMySql($db);

           // Handle telegram getUpdates request
           $telegram->handleGetUpdates();

           // $chat_id = 484577829;
           // $result = Request::sendMessage(['chat_id' => $chat_id, 'text' => 'Your utf8 text 😜 ...']);

           $results = Request::sendToActiveChats(
               'sendMessage', // Callback function to execute (see Request.php methods)
               ['text' => 'Hey! Check out the new features!!'], // Param to evaluate the request
               [
                   'groups'      => true,
                   'supergroups' => true,
                   'channels'    => false,
                   'users'       => true,
               ]
           );

           // Set webhook
           $result = $telegram->setWebhook($hook_url);
           // $result = $telegram->setWebhook($hook_url, ['certificate' => '/path/to/certificate']);
           if ($result->isOk()) {
               echo $result->getDescription();
           }
           // $result = Request::sendMessage(['chat_id' => $chat_id, 'text' => 'Your utf8 text 😜 ...']);

       } catch (Longman\TelegramBot\Exception\TelegramException $e) {
           // log telegram errors
           // echo $e->getMessage();
       }

        echo "Ok";
    }

    public function actionTelegram_hook()
    {
        $db = \Yii::$app->params['telegram']['db'];
        $bot_api_key  = \Yii::$app->params['telegram']['bot_api_key'];
        $bot_username = \Yii::$app->params['telegram']['bot_username'];
        $log_folder = 'log_telegram';

        $json = file_get_contents("php://input");
        $fp = fopen($log_folder.'/telegram.json', 'w');
        fwrite($fp, print_r($json, TRUE));
        fclose($fp);
        $json = preg_replace( "/\p{Cc}*$/u", "", $json);
        $json = json_decode($json, TRUE);

        try {
            // Create Telegram API object
            $telegram = new Telegram($bot_api_key, $bot_username);
            TelegramLog::initUpdateLog($log_folder . '/' . $bot_username . '_update.log');

            // Enable MySQL
            $telegram->enableMySql($db);

            // Handle telegram getUpdates request
            $telegram->handleGetUpdates();

            $chat_id = $json['message']['chat']['id'];
            // $command = $json['message']['text'];

            $entities = isset($json['message']['entities']) ? true : false;
            $is_command = [false];
            if($entities && $json['message']['entities'][0]['type'] == 'bot_command') {
                $is_command = explode(' ',$json['message']['text']);
            }

            // print_r($is_command);
            // exit();
            $answer = [];
            $answer[] = 'Nothing...';



            if($is_command[0] == '/start') {
                $answer = [];
                $answer[] = "Commands:";
                $answer[] = "/auth";
                $answer[] = "/balance";

            } elseif($is_command[0] == '/auth') {
                $answer = [];
                $lng = count($is_command);
                if($lng == 1) {
                    $answer[] = 'Input email & password.';
                    $answer[] = '/auth example@gmail.com mypassword';
                } elseif($lng == 3) {
                    // $answer[] = $is_command[1];

                    $email = $is_command[1];
                    $password = $is_command[2];

                    $validator = new \yii\validators\EmailValidator();
                    if (!$validator->validate($email, $error)) {
                        $answer[] = "Email isn't valid";
                    }

                    if(strlen($password) < 6 || strlen($password) > 30) {
                        $answer[] = "Password must be between 6 and 30 characters";
                    }

                    $query = User::find()->where(['email' => $email])->one();
                    $query->telegram_chat = $chat_id;
                    $query->save();

                    // print_r(count($query));
                    if(count($query)>0) {
                        $user_id = $query->id;
                        $answer[] = "You are logged!";
                        $answer[] = "/balance";
                    } else {
                        $answer[] = "Account not found";
                    }

                }

            } elseif($is_command[0] == '/balance') {

                // $chat_id = 484577829;
                $query = User::find()->where(['telegram_chat' => $chat_id])->one();

                $answer = [];
                $divider = '—————————————';

                if(count($query) > 0) {

                    $balanceArr = $this->getBalanceForTelegram($query->id);
                    $answer = [];
                    $answer[] = "BALANCE";
                    $updated_at = 0;
                    foreach ($balanceArr as $stock) {
                        $stock_name = strtoupper($stock['stock_name']);
                        $answer[] = $divider;
                        $answer[] = "[".$stock_name."]";
                        foreach ($stock['balance'] as $balance) {
                            $answer[] = "<b>".$balance['coin_label'].":</b> ".round($balance['available'], 8)." = $".round($balance['price_usd'], 2);
                            $updated_at = $balance['updated_at'];
                        }
                    }
                    $answer[] = $divider;
                    $answer[] = "<b>Updated:</b> ".dateConverter($updated_at);
                }


            }


            print_r($answer);
            $answer = join(PHP_EOL, $answer);
            // $answer = urlencode($answer);
            $result = Request::sendMessage(['chat_id' => $chat_id, 'text' => $answer, 'parse_mode'  => 'html']);



        } catch (Longman\TelegramBot\Exception\TelegramException $e) {
            // log telegram errors
            // echo $e->getMessage();
        }

    }

    public function actionTelegram_equalizer()
    {
        $modelUser = User::find()->where(['IS NOT', 'telegram_chat', null])->asArray()->all();
        $ordersCombWithProfit = $this->actionEqualizer_return();

        // print_r($modelUser);
        foreach ($modelUser as $user) {

            $db = \Yii::$app->params['telegram']['db'];
            $bot_api_key  = \Yii::$app->params['telegram']['bot_api_key'];
            $bot_username = \Yii::$app->params['telegram']['bot_username'];

            try {
                $telegram = new Telegram($bot_api_key, $bot_username);
                $telegram->enableMySql($db);
                $telegram->handleGetUpdates();

                $divider = PHP_EOL.'—————————————'.PHP_EOL;
                $dividerTime = dateConverter(time());

                $answerCombs = [
                    '🤑 <a href="'.Yii::$app->request->hostInfo.'/stocks/equalizer">'.$dividerTime.'</a> 🤑',
                ];

                $filteringCombs = $this->equalizer_with_filters($user['id'], $ordersCombWithProfit);

                foreach ($filteringCombs as $comb) {
                    $pair_coin_from = $comb['sell']['data']['pair']['coin_from'];

                    $sell_stock_name = $comb['sell']['data']['stock_name'];
                    $sell_coin_to = $comb['sell']['data']['pair']['coin_to'];
                    $buy_stock_name = $comb['buy']['data']['stock_name'];
                    $buy_coin_to = $comb['buy']['data']['pair']['coin_to'];
                    $total_volume = $comb['profit']['stat']['total_volume'];
                    $total_profit_usd = $comb['profit']['stat']['total_profit_usd'];
                    $median = $comb['profit']['stat']['median'];

                    $total_volume = number_format($total_volume, 0, ',', ' ');
                    $total_profit_usd = number_format($total_profit_usd, 0, ',', ' ');

                    $answerComb = [
                        '<b>'.round($median, 2).'% '.$pair_coin_from.'</b>',
                        $sell_coin_to.' → '.$buy_coin_to,
                        '('.$sell_stock_name.' → '.$buy_stock_name.')',
                        '<b>+ $'.$total_profit_usd.'</b> from $'.$total_volume,
                    ];
                    $answerCombs[] = join(PHP_EOL, $answerComb);

                }

                if(count($filteringCombs) > 0) {
                    $answer = join($divider, $answerCombs);
                    $result = Request::sendMessage(['chat_id' => $user['telegram_chat'], 'text' => $answer, 'parse_mode'  => 'html']);
                }

            } catch (Longman\TelegramBot\Exception\TelegramException $e) {
                // log telegram errors
                // echo $e->getMessage();
            };


            echo "Done!";
        }
    }

    public function getBalanceForTelegram($user_id)
    {

        $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
        $modelStockAccounts = StockAccount::find()->where(['user_id' => $user_id, 'deleted' => 0])->with(['balance','stock'])->asArray()->all();

        $balanceArr = [];

        if(count($modelStockAccounts) > 0) {

            foreach ($modelStockAccounts as $account) {

                $stock_name = $account['stock']['name'];
                $balanceArr[$stock_name] = [
                    "stock_name" => $stock_name,
                    "balance" => []
                ];

                foreach ($account['balance'] as $balance) {
                    foreach ($modelCoinmarketcap as $coinmarketcap) {
                        if($coinmarketcap['coin_label'] == $balance['coin_label']) {
                            $price_usd = $balance['available'] * $coinmarketcap['price_usd'];
                            $price_btc = $balance['available'] * $coinmarketcap['price_btc'];

                            if($price_usd >= 1) {
                                $balanceArr[$stock_name]['balance'][] = [
                                    "coin_label" => $balance['coin_label'],
                                    "available" => $balance['available'],
                                    "price_usd" => $price_usd,
                                    "price_btc" => $price_btc,
                                    "updated_at" => $balance['updated_at'],
                                ];
                            }

                        }
                    }
                }
            }
        }

        return $balanceArr;
    }

}



function fastLinkModifier($link, $coin_from, $coin_to) {
    // $link = "https://liqui.io/#/exchange/|coin_from=upper|divider=_|coin_to=upper";
    $newLink = false;
    if(strlen($link)>0) {
        $arr = explode("|", $link);
        $newLink = $arr[0];

        foreach ($arr as $str) {

            if (strpos($str, 'coin_from') !== false) {
                $str_tmp = explode("=", $str);
                if($str_tmp[1] == "upper") {
                    $coin_from = strtoupper($coin_from);
                } elseif($str_tmp[1] == "lower") {
                    $coin_from = strtolower($coin_from);
                }
                $newLink = $newLink.$coin_from;
            }
            if (strpos($str, 'divider') !== false) {
                $str_tmp = explode("=", $str);
                $newLink = $newLink.$str_tmp[1];
            }
            if (strpos($str, 'coin_to') !== false) {
                $str_tmp = explode("=", $str);
                if($str_tmp[1] == "upper") {
                    $coin_to = strtoupper($coin_to);
                } elseif($str_tmp[1] == "lower") {
                    $coin_to = strtolower($coin_to);
                }
                $newLink = $newLink.$coin_to;
            }

            // print_r($arr);
        }
    }

    // print_r($newLink);
    // exit();

    return $newLink;
}

function getCurrencies() {

    $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );

    // $coin_to = 'USD';
    $url = 'https://api.coinmarketcap.com/v1/ticker/?limit=0';
    $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
    $json = json_decode($content, true);

    $arrCoins = [];
    foreach ($json as $key => $coin) {
        $coin_from = $coin['symbol'];
        // if($coin_from == 'USDT') {
        //     $arrCoins[$coin['symbol']] = [
        //         "coin" => $coin_from,
        //         "price_usd" => 1,
        //     ];
        // } else {
        //     $arrCoins[$coin['symbol']] = [
        //         "coin" => $coin_from,
        //         "price_usd" => $coin['price_usd'],
        //     ];
        // }

        $arrCoins[$coin['symbol']] = [
            "coin" => $coin_from,
            "price_usd" => $coin['price_usd'],
            "price_btc" => $coin['price_btc'],
        ];
        // $modelPairs = Pair::find()->where(['coin_from' => $coin_from, 'coin_to' => 'USDT'])->asArray()->one();



    }

    // print_r($arrCoins);
    // exit();

    return $arrCoins;
}



function median($array) {
    $count = count($array);
    sort($array);
    $mid = floor(($count-1)/2);
    // $avg = ($total_percent)?array_sum($total_percent)/$count:0;
    $median = ($array)?($array[$mid]+$array[$mid+1-$count%2])/2:0;

    return $median;
}

function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}

function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}
