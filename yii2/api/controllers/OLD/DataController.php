<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use api\models\Signup;
// use api\models\Users;
// use api\models\Language;
// use api\models\Token;

// use yii\db\Expression;
use api\models\Token;
use api\models\OrderDeal;
use api\models\OrderCase;
use api\models\OrderStrategy;
use api\models\Coin;
use api\models\Pair;
use api\models\Stock;
use api\models\StockAccount;
use api\models\StockPair;
// use api\models\StockOrder;
use api\models\User;



// use api\models\PlaylistsItems;
// use api\models\PlaylistsItemsParts;

// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en
// http://demohost.com:8888/signup?email=maaffa@adffdil.cfom&password=123&language=en
// http://demohost.com:8888/login?email=maaffa@fdil.cfom&password=123
// http://demohost.com:8888/token/KFwvIADhmDY7C06y3_77vSi9BgQMcc7J





header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");




function getOrders() {
    // $modelStock = Stock::find()->with(['order'])->asArray()->all();
    $modelStockPair = StockPair::find()->where(['whitelist' => 1])->with(['pair', 'new_orders'])->asArray()->all();
    $whitelistPairs = [];
    $bestOrders = [];
    // print_r($modelStockPair);
    // exit();

    // print_r($modelStockPair);
    // exit();

    foreach ($modelStockPair as $key => $pair) {

        $orders = $pair['new_orders'];
        $arr_ask = [];
        $arr_bid = [];



        foreach ($orders as $k => $order) {

            if($order['type'] == 0) { // Ask, Sell
                $arr_ask[] = [
                    "id" => $order['id'],
                    "type" => "Ask",
                    "type_id" => 0,
                    "pair_id" => $order['pair_id'],
                    "price" => $order['price'],
                    "volume" => $order['volume'],
                    "created_at" => time_elapsed_string('@'.$order['created_at']),
                    "pair_id" => $pair['pair']['id'],
                    "pair_label" => $pair['pair']['coin_from']."_".$pair['pair']['coin_to'],
                    "coin_from" => $pair['pair']['coin_from'],
                    "coin_to" => $pair['pair']['coin_to'],
                    "stock_id" => $order['stock_id'],
                    "stock_name" => $order['stock']['name'],
                    "stock_commission" => $order['stock']['commission'],
                ];
            } elseif($order['type'] == 1) { // Bid, Buy
                $arr_bid[] = [
                    "id" => $order['id'],
                    "type" => "Bid",
                    "type_id" => 1,
                    "pair_id" => $order['pair_id'],
                    "price" => $order['price'],
                    "volume" => $order['volume'],
                    "created_at" => time_elapsed_string('@'.$order['created_at']),
                    "pair_id" => $pair['pair']['id'],
                    "pair_label" => $pair['pair']['coin_from']."_".$pair['pair']['coin_to'],
                    "coin_from" => $pair['pair']['coin_from'],
                    "coin_to" => $pair['pair']['coin_to'],
                    "stock_id" => $order['stock_id'],
                    "stock_name" => $order['stock']['name'],
                    "stock_commission" => $order['stock']['commission'],
                ];
            }

        }



        // Asc sort
        usort($arr_ask,function($first,$second){
            return $first['price'] > $second['price'];
        });
        usort($arr_bid,function($first,$second){
            return $first['price'] < $second['price'];
        });

        $pairLabel = $pair['pair']['coin_from']."_".$pair['pair']['coin_to'];
        // $bestOrders[] = $arr_ask[0];
        // $bestOrders[] = $arr_bid[0];

        if($arr_ask && $arr_bid) {
            $bestOrders[] = $arr_ask[0];
            $bestOrders[] = $arr_bid[0];
        }

        // array_merge($arr_ask, $arr_bid);

        // $bestOrders['ask'][$pairLabel] = $arr_ask;
        // $bestOrders['bid'][$pairLabel] = $arr_bid;
        // if($key == 1) {
        //     print_r($bestOrders);
        //     exit();
        // }

    }

    // $bestOrders = array_merge($bestOrders);
    $mergeOrders = [];
    foreach ($bestOrders as $order) {
        $mergeOrders[] = $order;

    }
    // print_r($mergeOrders);
    // exit();
    return $mergeOrders;
}

class DataController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }


    /**
     * Displays JSON videos.
     *
     * @return string
     */

    public function actionIndex($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $modelUser = User::find($user_id)->with(['language'])->asArray()->one();;
                $modelCase = OrderCase::find()->asArray()->all();
                $modelStock = Stock::find()->with(['withdrawal'])->asArray()->all();
                $modelStrategy = OrderStrategy::find()->asArray()->all();
                $modelAccount = StockAccount::find()->where(['user_id' => $user_id])->with(['stock'])->asArray()->all();
                $modelDealCount = OrderDeal::find()->where(['user_id' => $user_id])->asArray()->count();
                // $modelStockOrder = StockOrder::find()->with(['pair_usdt'])->asArray()->all();
                // print_r($modelDeal);
                // exit();

                // $modelCoin = Coin::find()->asArray()->all();


                $arrUser = [
                    "id" => $modelUser['id'],
                    "name" => $modelUser['name'],
                    "email" => $modelUser['email'],
                    "language" => $modelUser['language']['iso'],
                ];


                $arrAccount = [];
                foreach ($modelAccount as $key => $account) {
                    $arrAccount[] = [
                        "id" => $account['id'],
                        "name" => $account['name'],
                        "email" => $account['email'],

                        "label" => $account['stock']['name']." — ".$account['name'],
                        "stock_id" => $account['stock']['id'],
                        "stock_name" => $account['stock']['name'],
                        "stock_commission" => $account['stock']['commission'],

                        "created_at" => $account['created_at'],
                        "updated_at" => $account['updated_at'],
                    ];
                }
                // print_r($arrAccount);
                // exit();

                // $modelAccount = StockAccount::find()->where(['user_id' => $user_id])->with(['stock'])->asArray()->all();

                // $arrAccounts = [];
                // foreach ($modelAccount as $key => $account) {
                //     $arrAccounts[] = [
                //
                //         // "label" => $strategy['name'],
                //
                //     ];
                // }
                //
                $arrCase = [];
                foreach ($modelCase as $key => $case) {
                    $arrCase[] = $case;
                }

                $arrStock = [];
                foreach ($modelStock as $key => $stock) {
                    $arrStock[] = [
                        "id" => $stock['id'],
                        "name" => $stock['name'],
                        "website" => $stock['website'],
                        "id" => $stock['id'],
                        "withdrawal" => []
                    ];
                    $arrStockId = count($arrStock)-1;
                    foreach ($stock['withdrawal'] as $k => $withdrawal) {
                        $arrStock[$arrStockId]['withdrawal'][] = [
                            "commission" => $withdrawal['commission'],
                            "coin" => $withdrawal['coin']['iso']
                        ];
                    }
                }



                $arrStrategy = [];
                foreach ($modelStrategy as $key => $strategy) {
                    $arrStrategy[] = $strategy;
                }


                // $lastOrders = getOrders();
                // print_r($lastOrders);
                // exit();




                $result = array (
                    "status" => 200,
                    "results" => [
                        "user" => $arrUser,
                        "accounts" => $arrAccount,
                        "cases" => $arrCase,
                        // "coins" => $queryCoin,
                        "stocks" => $arrStock,
                        "strategies" => $arrStrategy,
                        "counterDeals" => $modelDealCount,
                        "counterAccounts" => count($arrAccount),
                        "counterStocks" => count($arrStock),
                        // "orders" => getOrders(),
                    ]
                );
                // print_r($result);
                print_r( json_encode($result) );
                exit();
            }

        }

    }

    public function actionPairs($token=false, $account=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id && $account) {


                $modelAccount = StockAccount::find()->where(['id' => $account,'user_id' => $user_id])->with(['stock'])->asArray()->one();
                $stockId = $modelAccount['stock']['id'];
                // print_r($modelAccount);
                // exit();
                $modelPair = StockPair::find()->where(['stock_id' => $stockId, 'whitelist' => 1])->with(['pair'])->asArray()->all();
                // print_r($modelAccount['stock']['id']);
                // print_r($modelAccount);
                // exit();

                // $modelPair = Pair::find()->asArray()->all();
                $arrPair = [];
                foreach ($modelPair as $key => $pair) {
                    $arrPair[] = [
                        "stock_name" => $modelAccount['stock']['name'],
                        "pair_id" => $pair['pair']['id'],
                        "label" => $pair['pair']['coin_from']."—".$pair['pair']['coin_to'],

                    ];
                }
                // print_r($arrPair);
                // exit();
                print_r( json_encode($arrPair) );
                exit();
            }
        }
    }

    // public function actionStrategy($token=false)
    // {
    //     if($token) {
    //         $user_id = Token::getUser($token);
    //         if($user_id) {
    //             $modelStrategy = OrderStrategy::find()->asArray()->all();
    //             $arrStrategy = [];
    //             foreach ($modelStrategy as $key => $strategy) {
    //                 $arrStrategy[] = [
    //                     "id" => $strategy['id'],
    //                     "label" => $strategy['name'],
    //
    //                 ];
    //             }
    //             // print_r($arrPair);
    //             // exit();
    //             print_r( json_encode($arrStrategy) );
    //             exit();
    //         }
    //     }
    // }

    public function actionAccounts($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $modelAccount = StockAccount::find()->where(['user_id' => $user_id])->with(['stock'])->asArray()->all();
                // print_r($modelAccount);
                // exit();
                $arrAccounts = [];
                foreach ($modelAccount as $key => $account) {
                    $arrAccounts[] = [
                        "account_id" => $account['id'],
                        "label" => $account['stock']['name']." — ".$account['name'],
                        "stock_id" => $account['stock']['id'],
                        "stock_name" => $account['stock']['name'],
                        "stock_commission" => $account['stock']['commission'],
                        // "label" => $strategy['name'],

                    ];
                }
                // print_r($arrAccounts);
                // print_r($modelAccount);
                // exit();
                print_r( json_encode($arrAccounts) );
                exit();
            }
        }
    }


    public function actionStock_orders($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $result = array (
                    "status" => 200,
                    "results" => getOrders()
                );

                print_r( json_encode($result) );
                exit();

            }
        }
    }

    public function actionStock_pairs($token=false, $id=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                    if($id == 0) {
                        $modelPair = StockPair::find()->with(['pair'])->asArray()->all();
                        $modelStockCounter = Stock::find()->asArray()->count();
                    } else {
                        $modelPair = StockPair::find()->where(['stock_id' => $id])->with(['pair'])->asArray()->all();
                        $modelStockCounter = 1;
                    }


                    // print_r( $modelPair );
                    // exit();
                    $assignPairs = [];
                    foreach ($modelPair as $key => $pair) {
                        $pair_label = $pair['pair']['coin_from']."_".$pair['pair']['coin_to'];
                        $assignPairs[$pair_label][] = $pair;
                    }
                    $assignPairsStocks = [];
                    foreach ($assignPairs as $key => $stock_pair) {
                        if(count($stock_pair) == $modelStockCounter) {
                            $assignPairsStocks[] = [
                                "pair_label" => $key,
                                "pair_id" => false,
                                "stocks" => [],
                            ];
                            $assignPairsStocksId = count($assignPairsStocks)-1;
                            foreach ($stock_pair as $stock) {
                                $assignPairsStocks[$assignPairsStocksId]['pair_id'] = $stock['pair_id'];
                                $assignPairsStocks[$assignPairsStocksId]['stocks'][] = [
                                    "stock_name" => $stock['stock_name'],
                                    "whitelist_status" => $stock['whitelist'],
                                    "whitelist_id" => $stock['id'],
                                ];
                            }
                        }
                    }
                    // $modelStockCounter
                    $result = array (
                        "status" => 200,
                        "stock_name" => "All stocks...",
                        "results" => $assignPairsStocks,
                    );
                    //
                    print_r( json_encode($result) );
                    exit();
                // }


            }
        }
    }
    public function actionStock_pairs_save($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {
                // $array = explode(',',$array);

                $array = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($array, TRUE));
                fclose($fp);

                if($array == -1) {
                    $modelPair = StockPair::find()->asArray()->all();
                } else {
                    $data = json_decode($array, TRUE);
                    $modelPair = StockPair::find()->where(['id' => $data])->asArray()->all();
                }

                foreach ($modelPair as $key => $pair) {
                    $model = StockPair::findOne($pair['id']);
                    $model->whitelist = $model->whitelist ? 0 : 1;
                    $model->save();
                    //
                    // print_r($model);
                }


                // foreach ($modelPair as $key => $pair) {
                //     $model = StockPair::find();
                //     $model->user_id = $user_id;
                //     $model->stock_account_id = $deal['stock_account_id'];
                //     $model->pair_id = $pair_id;
                //     $model->volume = $deal['volume'];
                //     $model->price = $deal['price'];
                //     $model->profit = 0;
                //     $model->type = $deal['type'];
                //     $model->strategy_id = $deal['strategy_id'];
                //     $model->profit_min = $deal['profit_min'];
                //     $model->note = $deal['note'];
                //     $model->status = $deal['status_id'];
                //     $model->created_at = time();
                //     $model->updated_at = time();
                //     $model->save();
                //
                $respond = [
                    "status" => 200,
                    "message" => "Saved"
                ];
                print_r( json_encode($respond) );
                exit();
                // }

                // print_r($modelPair);
            }
        }
    }



    public function actionStock_whitecoin($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $modelPair = StockPair::find()->with(['pair','stock'])->asArray()->all();
                $modelStockCounter = Stock::find()->asArray()->count();


                // print_r( $modelPair );
                // exit();
                $assignPairs = [];
                foreach ($modelPair as $key => $pair) {
                    // print_r($pair);
                    $coin_from = $pair['pair']['coin_from'];
                    $stock_name = $pair['stock']['name'];
                    $assignPairs[$coin_from][$stock_name][] = $pair;
                }

                $assignPairsStocks = [];
                foreach ($assignPairs as $coin => $stock_pairs) {
                    $assignPairsStocks[] = [
                        "coin_from" => $coin,
                        "status" => false,
                        "stocks" => [],
                    ];
                    $coinId = count($assignPairsStocks)-1;

                    $count_pairs = 0;
                    $count_pairs_whitelist = 0;

                    foreach ($stock_pairs as $pair) {
                        foreach ($pair as $stock) {
                            $stock_name = $stock['stock']['name'];
                            $coin_to = $stock['pair']['coin_to'];
                            // $pair_label = $stock['pair']['coin_from']."_".$stock['pair']['coin_to'];
                            $coin_to = $stock['pair']['coin_to'];
                            $assignPairsStocks[$coinId]['stocks'][$stock_name]['stock_name'] = $stock_name;
                            $assignPairsStocks[$coinId]['stocks'][$stock_name]['pairs'][] = [
                                "coin_to" => $coin_to,
                                "whitelist" => (int)$stock['whitelist'],
                                "pair_id" => $stock['pair_id'],
                                "stock_name" => $stock['stock_name'],
                            ];
                            $count_pairs++;
                            if($stock['whitelist']) {
                                $count_pairs_whitelist++;
                            }
                        }
                    }
                    $statusMessage = "Checked ".$count_pairs_whitelist." from ".$count_pairs;
                    if($count_pairs_whitelist == $count_pairs) {
                        $statusMessage = "Checked all";
                    } elseif($count_pairs_whitelist == 0) {
                        $statusMessage = "Not checked";
                    }
                    $assignPairsStocks[$coinId]['status'] = $statusMessage;
                }

                // foreach ($assignPairsStocks as $key => $coin) {
                //     // $assignPairsStocks[$key]['status'] = 'Ok';
                //     foreach ($coin as $stock) {
                //         print_r($stock);
                //     }
                //     // print_r( count($coin['stocks']) );
                //     exit();
                // }

                usort($assignPairsStocks,function($first,$second){
                    return count($first['stocks']) < count($second['stocks']);
                });

                // print_r($assignPairsStocks);
                //     if(count($stock_pair) == $modelStockCounter) {
                //         $assignPairsStocks[] = [
                //             "coin_from" => $key,
                //             // "pair_id" => false,
                //             "stocks" => [],
                //         ];
                //         $assignPairsStocksId = count($assignPairsStocks)-1;
                //         foreach ($stock_pair as $stock) {
                //             $assignPairsStocks[$assignPairsStocksId]['pair_id'] = $stock['pair_id'];
                //             $assignPairsStocks[$assignPairsStocksId]['stocks'][] = [
                //                 "stock_name" => $stock['stock_name'],
                //                 "whitelist_status" => $stock['whitelist'],
                //                 "whitelist_id" => $stock['id'],
                //             ];
                //         }
                //     }
                // }
                // $modelStockCounter
                $result = array (
                    "status" => 200,
                    "stock_name" => "All stocks...",
                    "results" => $assignPairsStocks,
                );
                //
                print_r( json_encode($result) );
                exit();


            }
        }
    }


    public function actionStock_whitecoin_save($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $array = file_get_contents("php://input");
                $fp = fopen('array.json', 'w');
                fwrite($fp, print_r($array, TRUE));
                fclose($fp);

                $json = json_decode($array, true);

                if($json) {

                    $modelPair = StockPair::find()->with(['pair'])->asArray()->all();

                    $tmpByCoinFrom = [];
                    foreach ($modelPair as $key => $pair) {
                        $coin_from = $pair['pair']['coin_from'];
                        $tmpByCoinFrom[$coin_from][] = $pair;
                    }

                    foreach ($json as $j) {
                        foreach ($tmpByCoinFrom as $key => $pairs) {
                            if($key == $j) {
                                // print_r($j);

                                $whitelistCounter = 0;
                                foreach ($pairs as $pair) {
                                    if($pair['whitelist']) {
                                        $whitelistCounter++;
                                    }
                                }

                                $whitelistStatus = $whitelistCounter > 0 ? 1 : 1;
                                if(count($pairs) == $whitelistCounter) {
                                    $whitelistStatus = 0;
                                }
                                foreach ($pairs as $pair) {
                                    $id = $pair['id'];
                                    $model = StockPair::findOne($id);
                                    $model->whitelist = $whitelistStatus;
                                    $model->save();
                                }
                            }

                        }
                    }

                    $respond = [
                        "status" => 200,
                        "message" => "Saved"
                    ];
                    print_r( json_encode($respond) );
                    exit();
                } else {
                    $respond = [
                        "status" => 200,
                        "message" => "Not post request"
                    ];
                    print_r( json_encode($respond) );
                    exit();
                }

            }
        }
    }


}


function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}

function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}

function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
