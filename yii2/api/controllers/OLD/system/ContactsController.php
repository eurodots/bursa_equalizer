<?php

namespace api\controllers\system;

use Yii;
// use yii\filters\AccessControl;
use yii\web\Controller;
// use yii\web\Response;
// use yii\filters\VerbFilter;




header('Access-Control-Allow-Origin: *');

class ContactsController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */





    public function actionIndex($action=false)
    {

        jsonOutput([
            'response' => [
                'email' => [
                    'label' => 'support@kupi.ru',
                    'link' => 'mailto:support@kupi.ru'
                ],
                'issues' => [
                    'label' => 'GitHub Issues',
                    'link' => 'https://github.com',
                ],
            ],
        ], 200);

        exit();

    }


}



function jsonOutput($arrays=false, $status=200, $message=false) {
    $json = [
        "status" => $status,
        "message" => $message,
    ];
    if($arrays) {
        foreach ($arrays as $key => $arr) {
            $json[$key] = $arr;
        }
    }

    print_r( json_encode($json) );
    exit();
}
