<?php

namespace api\controllers\system;

use Yii;
// use yii\filters\AccessControl;
use yii\web\Controller;
// use yii\web\Response;
// use yii\filters\VerbFilter;

// use api\models\Coin;
// use api\models\StockAccount;
// use api\models\StockAccountBalance;
use api\models\Stock;
use api\models\StockData;
// use api\models\StockOrder;
// use api\models\StockHistory;




header('Access-Control-Allow-Origin: *');

class HomeController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */





    public function actionIndex($action=false)
    {
        echo "ok";

        exit();

    }


    public function actionTable_stocks($action=false)
    {

        $modelStocks = Stock::find()->with(['data','pairs_count'])->asArray()->all();

        $stocks_arr = [];

        // create data
        // foreach ($modelStocks as $stock) {
        //     $model = new StockData();
        //     $model->stock_id = $stock['id'];
        //     $model->count_orders = 0;
        //     $model->api_orders = 0;
        //     $model->api_trading = 0;
        //     $model->save();
        // }

        foreach ($modelStocks as $stock) {

            $pairs_count = count($stock['pairs_count']);
            if($pairs_count == 0) { $pairs_count = '—'; }

            $stocks_arr[] = [
                'stock_id' => (int)$stock['id'],
                'stock_name' => ucwords($stock['name']),
                'website' => 'https://'.$stock['website'],
                'data' => [
                    // 'count_orders' => $stock['data']['count_orders'],
                    'pairs_count' => $pairs_count,
                    'api_orders' => $stock['data']['api_orders'] ? true : false,
                    'api_trading' => $stock['data']['api_trading'] ? true : false,
                ],
                'api' => [
                    'stock' => getCurrentPath('/api/v1/stocks/').$stock['name'],
                ],
            ];
        }

        usort($stocks_arr,function($first,$second){
            return $first['data']['api_orders'] < $second['data']['api_orders'];
        });
        // exit

        jsonOutput([
            'response' => $stocks_arr,
        ], 200);

        exit();

    }

}


// function stockArrView($stock) {
//     return [
//         'stock_id' => (int)$stock['id'],
//         'stock_name' => $stock['name'],
//         'website' => 'https://'.$stock['website'],
//         'commission_percent' => (double)$stock['commission'],
//         'api' => [
//             'stock' => getCurrentPath().'stocks/'.$stock['name'],
//             'stock_with_fastlink' => getCurrentPath().'stocks/'.$stock['name'].'?fastlink=ETH_BTC',
//             'orders' => getCurrentPath().'stocks/'.$stock['name'].'/orders',
//             'trading_pairs' => getCurrentPath().'stocks/'.$stock['name'].'/trading_pairs',
//             'history' => getCurrentPath().'stocks/'.$stock['name'].'/history',
//             'pairs' => getCurrentPath().'stocks/'.$stock['name'].'/pairs',
//             'chart' => getCurrentPath().'stocks/'.$stock['name'].'/chart?pair=ETH_BTC',
//         ],
//         'meta' => [
//             'active' => false,
//             'fast_link' => false,
//         ],
//     ];
// }

function jsonOutput($arrays=false, $status=200, $message=false) {
    $json = [
        "status" => $status,
        "message" => $message,
    ];
    if($arrays) {
        foreach ($arrays as $key => $arr) {
            $json[$key] = $arr;
        }
    }

    print_r( json_encode($json) );
    exit();
}


function timeStamp($format=false) {

    $timezone = \Yii::$app->params['timezone'];
    $date = new \DateTime('now', new \DateTimeZone($timezone));

    if($format) {
        // $format = 'd-m-Y H:i:s'
        $date = $date->format($format);
    } else {
        $date = $date->getTimestamp();
    }

    return $date;
}


function epochToDate($epoch, $format=false) {

    $timezone = \Yii::$app->params['timezone'];
    $date = new \DateTime("@$epoch");
    $date->setTimezone(new \DateTimeZone($timezone));

    if($format) {
        $date = $date->format($format);
    } else {
        $date = $date->format('Y-m-d H:i:sP');
    }

    return $date;
}

function getCurrentPath($path=false) {
    $current_path = Yii::$app->request->hostInfo;
    if($path) {
        $current_path = $current_path.$path;
    } else {
        $current_path = $current_path.'/api/system/';
    }
    return $current_path;
}
