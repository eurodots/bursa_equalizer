<?php

namespace api\controllers\api1;

use Yii;
use yii\web\Controller;

use api\models\Coins;
use api\models\CoinsHistory;

header('Access-Control-Allow-Origin: *');

class CoinsController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */



     public function actionIndex($action=false)
     {

         $modelCoins = Coins::find()->with(['links'])->asArray()->all();

         $data_arr = [];
         $coins_arr = [];
         foreach ($modelCoins as $coin) {

             // print_r($coin);
             // exit();

             if($coin['platform'] == 'Ethereum' && strpos($coin['contract_address'], '0x') !== false) {
                 $coin_name = $coin['coin'];
                 $coins_arr[] = $coin_name;
                 $data_arr[$coin_name] = coinTableRowView($coin);
                 $coinHistory = CoinsHistory::find()
                    ->where(['id' => $coin['id']])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()->one();


                $data_arr[$coin_name]['history'] = [
                    'capital_usd' => $coinHistory['capital_usd'],
                    'price_usd' => $coinHistory['price_usd'],
                    'volume24_usd' => $coinHistory['volume24_usd'],
                    'suply_number' => $coinHistory['suply_number'],
                    'changes_24h' => $coinHistory['changes_24h'],
                ];
                // $data_arr[$coin_name]['rank_max'] = $maxRank;
             }


         }
         // print_r($coins_arr);
         // exit();


        jsonOutput([
            'response' => [
                'data' => $data_arr,
                'coins' => $coins_arr,
            ],
        ], 200);

     }


     public function actionView_token($init=false)
     {
         $modelCoin = false;

         if(strlen($init) < 10) {
             $init = strtoupper($init);
             $modelCoin = Coins::find()->where(['coin' => $init])->with(['links'])->asArray()->one();
         } else {
             $init = strtolower($init);
             $modelCoin = Coins::find()->where(['contract_address' => $init])->with(['links'])->asArray()->one();
         }

         if($modelCoin) {
             $view = coinArrView($modelCoin);
             $view['history'] = CoinsHistory::find()
                ->where(['id' => $modelCoin['id']])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()->one();

            $modelRank = Coins::find()
               ->orderBy(['rank' => SORT_DESC])
               ->asArray()->one();

            $view['rank_max'] = $modelRank['rank'];

            jsonOutput([
                'response' => [
                    'data' => $view,
                ],
            ], 200);

         } else {
             jsonOutput(false, 300, 'Token not found!');
         }

         exit();
     }

}



function coinTableRowView($coin) {
    $coin_name = $coin['coin'];

    $coins_arr = [
        'id' => $coin['id'],
        'coin' => $coin['coin'],
        'label' => $coin['label'],
        // 'rank' => $coin['rank'],
        // 'rank_max' => false,
        'contract_address' => $coin['contract_address'],
        // 'coinmarketcap' => $coin['coinmarketcap'],
        // 'updated_at' => $coin['updated_at'],
        'updated_date' => epochToDate($coin['updated_at']),
        'favicon32' => getCurrentRoute().'/api/web/favicons/32x32/'.$coin_name.'.png',
        'active' => false,
        // 'favicon128' => getCurrentRoute().'/api/web/favicons/128x128/'.$coin_name.'.png',
        // 'links' => [],
    ];

    return $coins_arr;
}

function coinArrView($coin) {
    $coin_name = $coin['coin'];

    $coins_arr = [
        'id' => $coin['id'],
        'coin' => $coin['coin'],
        'label' => $coin['label'],
        'rank' => $coin['rank'],
        'rank_max' => false,
        'contract_address' => $coin['contract_address'],
        'coinmarketcap' => $coin['coinmarketcap'],
        'updated_at' => $coin['updated_at'],
        'updated_date' => epochToDate($coin['updated_at']),
        'favicon32' => getCurrentRoute().'/api/web/favicons/32x32/'.$coin_name.'.png',
        'favicon128' => getCurrentRoute().'/api/web/favicons/128x128/'.$coin_name.'.png',
        'links' => [],
    ];

    $links_arr = [];
    foreach ($coin['links'] as $link) {
        $links_arr[] = [
            'label' => $link['label'],
            'value' => $link['value'],
        ];
    }
    $coins_arr['links'] = $links_arr;

    return $coins_arr;
}


function jsonOutput($arrays=false, $status=200, $message=false) {
    $json = [
        "status" => $status,
        "message" => $message,
    ];
    if($arrays) {
        foreach ($arrays as $key => $arr) {
            $json[$key] = $arr;
        }
    }

    print_r( json_encode($json) );
    exit();
}

function epochToDate($epoch, $format=false) {

    $timezone = \Yii::$app->params['timezone'];
    $date = new \DateTime("@$epoch");
    $date->setTimezone(new \DateTimeZone($timezone));

    if($format) {
        $date = $date->format($format);
    } else {
        $date = $date->format('Y-m-d H:i:sP');
    }

    return $date;
}

function getCurrentPath() {
    return Yii::$app->request->hostInfo.'/api/v1/';
}


function getCurrentRoute() {
    return Yii::$app->request->hostInfo;
}


/*
function dateToEpochConverter($date=false, $format='!d-m-Y H:i:s') {

    if (\DateTime::createFromFormat($format, $date) !== FALSE) {
        return \DateTime::createFromFormat($format, $date)->getTimestamp();
    } else {
        jsonOutput(false, 400, 'Wrong date!');
    }

    // try {
    //     $date =
    //     return $date->getTimestamp();
    // } catch (\Throwable $t) {
    //     jsonOutput(false, 400, 'Wrong date!');
    // }
}


function stockArrView($stock) {
    return [
        'stock_id' => (int)$stock['id'],
        'stock_name' => $stock['name'],
        'website' => 'https://'.$stock['website'],
        'commission_percent' => (double)$stock['commission'],
        'api' => [
            'stock' => getCurrentPath().'stocks/'.$stock['name'],
            'stock_with_fastlink' => getCurrentPath().'stocks/'.$stock['name'].'?fastlink=ETH_BTC',
            'orders' => getCurrentPath().'stocks/'.$stock['name'].'/orders',
            'trading_pairs' => getCurrentPath().'stocks/'.$stock['name'].'/trading_pairs',
            'history' => getCurrentPath().'stocks/'.$stock['name'].'/history',
            'pairs' => getCurrentPath().'stocks/'.$stock['name'].'/pairs',
            'chart' => getCurrentPath().'stocks/'.$stock['name'].'/chart?pair=ETH_BTC',
        ],
        'meta' => [
            'active' => false,
            'fast_link' => false,
        ],
    ];
}

function numberDeterminer($value=false) {
    if($value) {

        $format = 0;

        if($value > 100) {
            $format = 2;
        } elseif($value > 10) {
            $format = 2;
        } elseif($value > 1) {
            $format = 2;
        } elseif($value > 0.1) {
            $format = 4;
        } elseif($value > 0.01) {
            $format = 5;
        } elseif($value > 0.001) {
            $format = 5;
        } elseif($value > 0.0001) {
            $format = 5;
        } elseif($value > 0.00001) {
            $format = 6;
        } elseif($value > 0.000001) {
            $format = 7;
        } elseif($value > 0.0000001) {
            $format = 8;
        } elseif($value > 0.00000001) {
            $format = 9;
        }


        $explode = explode('.', (string)$value);
        if(!isset($explode[1])) {
            $explode[1] = '00';
        }

        $explode[1] = substr($explode[1], 0, $format);

        $new_value = join('.', $explode);
        $new_value = (double) $new_value;

        return $new_value;
    }

}


function exchangeRatesArrView($arr) {

    // print_r($arr);
    // exit();
    $arrConstruct = [
        'coin_id' => (int)$arr['coin_id'],
        'coin_label' => $arr['coin_label'],
        'coin_name' => $arr['name'],
        'rank' => (int)$arr['rank'],
        'percent_change_1h' => (double)$arr['percent_change_1h'],
        'percent_change_24h' => (double)$arr['percent_change_24h'],
        'percent_change_7d' => (double)$arr['percent_change_7d'],
        'price_usd' => numberDeterminer($arr['price_usd']),
        'price_btc' => numberDeterminer($arr['price_btc']),
        'updated_at' => (int)$arr['updated_at'],
        'updated_date' => epochToDate($arr['updated_at']),
        'link' => getCurrentPath().'rates?coin='.$arr['coin_label'],
    ];

    return $arrConstruct;
}

function DB_get_coin($coin=false) {

    if($coin) {
        $modelCoin = Coin::find()->where(['iso' => $coin])->asArray()->one();
        if($modelCoin) {
            return $modelCoin;
        } else {
            jsonOutput(false, 400, 'Variable $coin not found');
        }
    } else {
        jsonOutput(false, 400, 'Variable $coin not specified!');
    }

}


function DB_get_pair($pair=false) {

    if($pair) {
        $pair_arr = explode('_', $pair);
        if(count($pair_arr) == 2) {
            // print_r($pair_arr);
            $modelPair = Pair::find()->where(['coin_from' => $pair_arr[0], 'coin_to' => $pair_arr[1]])->asArray()->one();
            if($modelPair) {
                return $modelPair;
            } else {
                jsonOutput(false, 400, 'Variable $pair not found');
            }

        } else {
            jsonOutput(false, 400, 'Wrong format of variable $pair!');
        }

    } else {
        jsonOutput(false, 400, 'Variable $pair not specified!');
    }
}

function DB_get_stock($stock_name) {

    if($stock_name) {
        $modelStock = Stock::find()->where(['name' => $stock_name])->asArray()->one();
        if($modelStock) {
            return $modelStock;
        } else {
            jsonOutput(false, 400, 'Stock not found');
        }
    } else {
        jsonOutput(false, 400, 'Stock not found');
    }
}

function fastLinkModifier($link, $coin_from, $coin_to) {
    // $link = "https://liqui.io/#/exchange/|coin_from=upper|divider=_|coin_to=upper";
    $newLink = false;
    if(strlen($link)>0) {
        $arr = explode("|", $link);
        $newLink = $arr[0];

        foreach ($arr as $str) {

            if (strpos($str, 'coin_from') !== false) {
                $str_tmp = explode("=", $str);
                if($str_tmp[1] == "upper") {
                    $coin_from = strtoupper($coin_from);
                } elseif($str_tmp[1] == "lower") {
                    $coin_from = strtolower($coin_from);
                }
                $newLink = $newLink.$coin_from;
            }
            if (strpos($str, 'divider') !== false) {
                $str_tmp = explode("=", $str);
                $newLink = $newLink.$str_tmp[1];
            }
            if (strpos($str, 'coin_to') !== false) {
                $str_tmp = explode("=", $str);
                if($str_tmp[1] == "upper") {
                    $coin_to = strtoupper($coin_to);
                } elseif($str_tmp[1] == "lower") {
                    $coin_to = strtolower($coin_to);
                }
                $newLink = $newLink.$coin_to;
            }

        }
    }

    return $newLink;
}






function getFilesContent($path) {
    try {
        return json_decode(file_get_contents($path), true);
    } catch (\Exception $e) {
        jsonOutput(false, 400, 'Not found');
    }
}


function mergeArrays($arrays) {
    $stocks_arr = [];
    foreach ($arrays as $key_stock_name => $stocks) {

        $stock_merge_arr = [];
        foreach ($stocks as $array) {
            $stock_merge_arr = $stock_merge_arr + $array;
        }

        $stocks_arr[$key_stock_name] = $stock_merge_arr;

    }
    return $stocks_arr;
}



function getDirReverse($path) {
    if(is_dir($path)) {
        $rii = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));

        $files = array();
        foreach ($rii as $file) {
            if (!$file->isDir()) {
                $path_arr = $file->getPathname();
                $path_arr = explode('/', $path_arr);
                // $files[] = $path_arr;
                $path_arr = array_reverse($path_arr);
                $path_arr['filename'] = explode('.', $path_arr[0]);
                // $filename_arr = explode('.', $path_arr[0]);
                $files[] = $path_arr;
                // $files[] = array_slice($path_arr, 0, 3);
            }
        }
        // if (isset($files[0])) {
        //     print_r($files[0]);
        //     exit();
        //     // $files[0] = explode('.', $files[0]);
        // }


        return $files;
    } else {
        jsonOutput(false, 400, 'Stock not found');
    }

}

function timeStamp($format=false) {

    $timezone = \Yii::$app->params['timezone'];
    $date = new \DateTime('now', new \DateTimeZone($timezone));

    if($format) {
        // $format = 'd-m-Y H:i:s'
        $date = $date->format($format);
    } else {
        $date = $date->getTimestamp();
    }

    return $date;
}




function getRootUrl() {
    return \Yii::$app->request->hostInfo;
}

function getCurrentUrl() {
    $hostInfo = \Yii::$app->request->hostInfo;
    $urlPath = \Yii::$app->request->getUrl();
    $urlPath = explode('?', $urlPath);

    $path = $hostInfo.$urlPath[0];

    return $path;
}

function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}

*/
