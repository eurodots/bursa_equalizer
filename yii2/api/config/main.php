<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'timeZone' => 'Europe/London',
    'components' => [
        'request' => [
            // 'csrfParam' => '_csrf-api',
            'parsers' => [
                'application/json' => 'yii\web\jsonParser'
            ],
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'cookieValidationKey' => '',
            'hostInfo' => 'http://bursadex.com:8888',
            'baseUrl'=>'/api',
        ],

        'response' => [
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\jsonResponseFormatter',
                    'prettyprint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                ]
            ]
        ],
        // 'session' => [
        //     // this is the name of the session cookie used for login on the api
        //     'name' => 'advanced-api',
        // ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',
                // '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',


                // *******//
                // API V1 //
                // *******//

                        // 'v1'                                   => 'api1/orders/api_info',
                        'v1/coins'                                => 'api1/coins/index',
                        'v1/coins/<init:\w+>'                     => 'api1/coins/view_token',
                        // 'v1/stocks'                            => 'api1/orders/stocks',
                        // 'v1/stocks/<stock_name:>'              => 'api1/orders/stock_info',
                        // 'v1/stocks/<stock_name:>/orders'       => 'api1/orders/stock_orders',
                        // 'v1/stocks/<stock_name:>/trading_pairs' => 'api1/orders/stock_trading_pairs',
                        //
                        // 'v1/stocks/<stock_name:>/history'      => 'api1/orders/stock_history',
                        // 'v1/stocks/<stock_name:>/pairs'        => 'api1/orders/stock_pairs',
                        // 'v1/stocks/<stock_name:>/chart'        => 'api1/orders/stock_chart',
                        //
                        // // CRON
                        // 'v1/cron'                              => 'api1/cron/cron_info',
                        // 'v1/cron/orders_to_json'               => 'api1/cron/orders_to_json',
                        // 'v1/cron/orders_to_history'            => 'api1/cron/orders_to_history',
                        // 'v1/cron/orders_to_backup'             => 'api1/cron/orders_to_backup',
                        //
                        // // 'v1/test'                              => 'api1/cron/test',
                        //
                        // // DONATION
                        // 'v1/donation'                          => 'api1/donation/index',
                        // 'v1/donation/<wallet:>'                => 'api1/donation/qr',


            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            // 'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        // 'errorHandler' => [
        //     'errorAction' => 'site/error',
        // ],
        'db' => [
            'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=yii2_test',
            'dsn' => 'mysql:host=localhost;dbname=bursa',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',

            // Schema cache options (for production environment)
            //'enableSchemaCache' => true,
            //'schemaCacheDuration' => 60,
            //'schemaCache' => 'cache',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
