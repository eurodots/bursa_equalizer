<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            // 'csrfParam' => '_csrf-api',
            'parsers' => [
                'application/json' => 'yii\web\jsonParser'
            ],
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'cookieValidationKey' => '',
            'baseUrl'=>'/user',
        ],
        'response' => [
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\jsonResponseFormatter',
                    'prettyprint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                ]
            ]
        ],
        // 'session' => [
        //     // this is the name of the session cookie used for login on the api
        //     'name' => 'advanced-api',
        // ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '' => 'site/index',
                // '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',

                'signup' => 'site/signup',
                'login' => 'site/login',
                'token/<token:>' => 'site/token',

                'playlists/<token:>/' => 'playlists/index',
                'save/<token:>/' => 'save/index',

                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',

                // 'playlists/<token:>/playlist_add' => 'playlists/playlist_add',
                // 'playlists/<token:>/playlist_hold' => 'playlists/playlist_hold',
                // 'playlists/<token:>/playlist_edit' => 'playlists/playlist_edit',
                // 'playlists/<token:>/playlist_delete' => 'playlists/playlist_delete',
                //
                // 'playlists/<token:>/search_item_by_video_id/<video_id:>' => 'playlists/search_item_by_video_id', //Search item by video_id
                //
                //
                // 'playlists/<token:>/add_item_and_part' => 'playlists/add_item_and_part',
                // 'playlists/<token:>/item_add' => 'playlists/item_add',
                // 'playlists/<token:>/item_change_playlist' => 'playlists/item_change_playlist',
                // 'playlists/<token:>/item_delete' => 'playlists/item_delete',
                //
                // 'playlists/<token:>/part_add' => 'playlists/part_add',
                // 'playlists/<token:>/part_change' => 'playlists/part_change',
                // 'playlists/<token:>/part_delete' => 'playlists/part_delete',

                // changeplaylist

                // 'playlists/<token:>' => 'playlists/index',
                // 'auth' => 'site/login',
                // '<_c:[\w-]+>' => '<_c>/index',
                // '<_c:[\w-]+>/<id:\id+>' => '<_c>/view',
                // '<_c:[\w-]+>/<id:\id+>/<_a:[\w-]+>' => '<_c>/<_a>',

                // 'GET playlists' => 'site/index',
                // 'PUT,PATCH' => 'site/index',

            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            // 'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        // 'errorHandler' => [
        //     'errorAction' => 'site/error',
        // ],
        'db' => [
            'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=yii2_test',
            'dsn' => 'mysql:host=localhost;dbname=playlists',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',

            // Schema cache options (for production environment)
            //'enableSchemaCache' => true,
            //'schemaCacheDuration' => 60,
            //'schemaCache' => 'cache',
        ]
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
