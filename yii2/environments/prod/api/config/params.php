<?php


$lastOrdersDateStr = '-2 minutes';
// $lastOrdersDateStr = '-15 minutes';
// $lastOrdersDateStr = '-5 days';

return [
    'adminEmail' => 'support@kupi.net',
    'reCaptchaKey' => '6Lcd_SQTAAAAANjmGrRmHi719tTg7p4cmwHn5eay',
    'globalCoins' => ['BTC', 'USDT', 'ETH', 'XRB', 'BNB', 'NZDT', 'DODGE', 'LTC'],
    'lastOrdersDateStr' => $lastOrdersDateStr,
    'lastOrdersDate' => strtotime($lastOrdersDateStr),
    'timezone' => 'GMT0',
    'wallets' => [
        'eth' => [
            'name' => 'Etherium',
            'address' => '0x964B14b1671eF35DE02F36bCdbE1a1468e81cDc6',
        ],
        'btc' => [
            'name' => 'Bitcoin',
            'address' => '1NTYtVrkriXR7jdbsGJsezfwUcPfmZhp7h',
        ],
    ],


    'telegram' => [
        "bot_api_key" => '488967294:AAF18ppCx14-db0uEjaUlos2pEiVCI6DZmk',
        "bot_username" => 'elsignal_bot',
        "db" => [
           'host'     => 'localhost',
           'user'     => 'root',
           'password' => 'root',
           'database' => 'telegram',
        ],
    ],
];
