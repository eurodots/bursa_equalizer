<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
// use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $user_id
 * @property string $expired_at
 * @property string $token
 */
class Token extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%token}}';
    }

    public function generateToken($expire)
    {
        this.expired_at = $expire;
        this.token = \Yii::app->security->generateRandomString();
    }

}
